package cat.jaumemoron.centers.persistence.configuration;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({PersistenceConfiguration.class})
@Documented
public @interface EnablePersistenceConfiguration {
}
