package cat.jaumemoron.centers.persistence.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@Configuration
@ComponentScan("cat.jaumemoron.centers.persistence")
@EnableElasticsearchRepositories(basePackages = "cat.jaumemoron.centers.persistence.repository")
@EnableCaching
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@EntityScan(basePackages = "cat.jaumemoron.centers.persistence.domain", basePackageClasses = Jsr310JpaConverters.class)
@EnableJpaRepositories(basePackages = "cat.jaumemoron.centers.persistence.repository")
public class PersistenceConfiguration {

}
