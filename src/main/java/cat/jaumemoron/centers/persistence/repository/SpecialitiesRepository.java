package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Speciality;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SpecialitiesRepository extends CrudRepository<Speciality, String> {

    List<Speciality> findAllByOrderByNameAsc();


}
