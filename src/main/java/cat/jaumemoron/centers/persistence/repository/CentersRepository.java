package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Center;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CentersRepository extends PagingAndSortingRepository<Center, String> {

    Page<Center> findByCityId(String cityId, Pageable pageable);

    Page<Center> findByRegionId(String regionId, Pageable pageable);

    Page<Center> findByNameContaining(String name, Pageable pageable);

    Page<Center> findByTerritorialId(String territorialId, Pageable pageable);

    Page<Center> findByNatureId(String natureIdId, Pageable pageable);

    Page<Center> findByOwnerId(String ownerId, Pageable pageable);

    long countByTerritorialId(String territorialId);

}
