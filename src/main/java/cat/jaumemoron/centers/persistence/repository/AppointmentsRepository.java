package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Appointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface AppointmentsRepository extends PagingAndSortingRepository<Appointment, String> {

    Page<Appointment> findBySeasonIdAndTerritorialId(String season, String territorialId, Pageable pageable);

    @Query(value = "SELECT a.SEASON_ID aid, t.ID tid, t.NAME tname, s.ID sid, s.NAME sname, a.NUMBER anumber, a.APPOINTMENT_DATE adate" +
            "    FROM APPOINTMENTS a, TERRITORIALS t, SPECIALITIES s " +
            "    WHERE (a.TERRITORIAL_ID, a.SPECIALITY_ID, a.NUMBER) IN " +
            "(SELECT a2.TERRITORIAL_ID, a2.SPECIALITY_ID, MAX(a2.NUMBER) " +
            "    FROM APPOINTMENTS a2 WHERE a2.SEASON_ID = :season " +
            "  GROUP BY a2.TERRITORIAL_ID, a2.SPECIALITY_ID) " +
            "    AND a.TERRITORIAL_ID = t.ID " +
            "    AND a.SPECIALITY_ID = s.ID " +
            "    AND a.SEASON_ID = :season", nativeQuery = true)
    List<Object[]> getMaxAppointmentNumber(@Param("season") String seasonId);

    @Query(value = "SELECT a.SEASON_ID aid, t.ID tid, t.NAME tname, s.ID sid, s.NAME sname, max(a.NUMBER) anumber, a.APPOINTMENT_DATE adate" +
            "  FROM APPOINTMENTS a, TERRITORIALS t, SPECIALITIES s " +
            " WHERE (a.TERRITORIAL_ID, a.SPECIALITY_ID, a.APPOINTMENT_DATE) IN " +
            "       (SELECT a2.TERRITORIAL_ID, a2.SPECIALITY_ID, MAX(a2.APPOINTMENT_DATE) " +
            "          FROM APPOINTMENTS a2 WHERE a2.SEASON_ID = :season" +
            "         GROUP BY a2.TERRITORIAL_ID, a2.SPECIALITY_ID) " +
            "   AND a.TERRITORIAL_ID = t.ID " +
            "   AND a.SPECIALITY_ID = s.ID " +
            "   AND a.SEASON_ID = :season" +
            "  GROUP BY  a.SEASON_ID, t.ID, t.NAME, s.ID, s.NAME, a.APPOINTMENT_DATE", nativeQuery = true)
    List<Object[]> getMaxAppointmentNumberFromLastDate(@Param("season") String seasonId);

    @Query(value = "SELECT a.SEASON_ID aid, t.ID tid, t.NAME tname, s.ID sid, s.NAME sname, a.NUMBER anumber, a.APPOINTMENT_DATE adate, a.DAYTYPE " +
            "    FROM APPOINTMENTS a, TERRITORIALS t, SPECIALITIES s " +
            "    WHERE a.DAYTYPE IS NOT NULL AND (a.TERRITORIAL_ID, a.SPECIALITY_ID, a.DAYTYPE, a.NUMBER) IN " +
            "(SELECT a2.TERRITORIAL_ID, a2.SPECIALITY_ID, a2.DAYTYPE, MAX(a2.NUMBER) " +
            "    FROM APPOINTMENTS a2 WHERE a2.SEASON_ID = :season " +
            "  GROUP BY a2.TERRITORIAL_ID, a2.SPECIALITY_ID, a2.DAYTYPE) " +
            "    AND a.TERRITORIAL_ID = t.ID " +
            "    AND a.SPECIALITY_ID = s.ID " +
            "    AND a.SEASON_ID = :season", nativeQuery = true)
    List<Object[]> getAppointmentDataGroupByTerritorialAndSpecialityAndDayType(@Param("season") String seasonId);

    Page<Appointment> findBySeasonIdAndTerritorialIdAndSpecialityId(String season, String territorialId, String specialityId, Pageable pageable);

    long countBySeasonId(String seasonId);

    @Query(value = "SELECT t.ID, t.NAME, COUNT(1) c " +
            "         FROM APPOINTMENTS a, TERRITORIALS t " +
            "        WHERE a.APPOINTMENT_DATE = ? AND t.ID = a.TERRITORIAL_ID" +
            "        GROUP BY t.ID, t.NAME", nativeQuery = true)
    List<Object[]> getTerritorialsCountByDate(Date date);

    @Query(value = "SELECT A.TERRITORIAL_ID, T.NAME, MAX(A.NUMBER) " +
            "         FROM APPOINTMENTS A, TERRITORIALS T " +
            "        WHERE (A.APPOINTMENT_DATE, A.TERRITORIAL_ID) IN (SELECT MAX(APPOINTMENT_DATE), TERRITORIAL_ID FROM APPOINTMENTS WHERE SEASON_ID = :season GROUP BY TERRITORIAL_ID) " +
            "          AND A.TERRITORIAL_ID = T.ID " +
            "          AND A.SEASON_ID = :season " +
            "        GROUP BY A.TERRITORIAL_ID, T.NAME ORDER BY A.TERRITORIAL_ID", nativeQuery = true)
    List<Object[]> getTerritorialsLastNumber(@Param("season") String seasonId);


    List<Appointment> findBySeasonIdAndTerritorialIdAndSpecialityIdAndNumber(String season, String territorialId, String specialityId, long number);

    Appointment findFirstBySeasonIdAndTerritorialIdAndSpecialityIdAndNumberLessThanOrderByNumberDesc(String season, String territorialId, String specialityId, long number);

    Appointment findFirstBySeasonIdAndTerritorialIdAndSpecialityIdAndNumberGreaterThanOrderByNumberAsc(String season, String territorialId, String specialityId, long number);

    long countBySeasonIdAndTerritorialIdAndSpecialityId(String season, String territorialId, String specialityId);


    @Query(value = "SELECT MONTH(APPOINTMENT_DATE), count(1) " +
            "         FROM APPOINTMENTS " +
            "        WHERE TERRITORIAL_ID = :territorialId AND SPECIALITY_ID = :specialityId " +
            "         AND SEASON_ID = :seasonId GROUP BY MONTH(APPOINTMENT_DATE)", nativeQuery = true)
    List<Object[]> monthAppointmentsBySeasonIdAndTerritorialIdAndSpecialityId(@Param("seasonId") String seasonId,
                                                                              @Param("territorialId") String territorialId,
                                                                              @Param("specialityId") String specialityId);

}
