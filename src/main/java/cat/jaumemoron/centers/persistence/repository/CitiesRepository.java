package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.City;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CitiesRepository extends PagingAndSortingRepository<City, String> {

    List<City> findAll(Sort sort);

}
