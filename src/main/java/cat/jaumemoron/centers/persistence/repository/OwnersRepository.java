package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Owner;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface OwnersRepository extends PagingAndSortingRepository<Owner, String> {

    List<Owner> findAll(Sort sort);

}
