package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.domain.AppointmentDataDayType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AppointmentDataDayTypeRepository extends PagingAndSortingRepository<AppointmentDataDayType, String> {

    Page<AppointmentDataDayType> findBySeasonAndTerritorialId(String season, String territorialId, Pageable pageable);

    Page<AppointmentDataDayType> findBySeasonAndTerritorialIdAndDayType(String season, String territorialId, DayType dayType, Pageable pageable);

}
