package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Type;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TypesRepository extends PagingAndSortingRepository<Type, String> {

    List<Type> findAll(Sort sort);

}
