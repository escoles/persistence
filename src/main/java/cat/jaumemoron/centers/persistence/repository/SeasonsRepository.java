package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Season;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SeasonsRepository extends CrudRepository<Season, String> {

    List<Season> findAllByOrderByIdDesc();


}
