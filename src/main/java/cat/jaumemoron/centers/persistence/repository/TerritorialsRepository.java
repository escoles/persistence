package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Territorial;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface TerritorialsRepository extends PagingAndSortingRepository<Territorial, String> {

    List<Territorial> findAll(Sort sort);

    Optional<Territorial> findByAppointmentId(String appointmentId);

}
