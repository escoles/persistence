package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.AppointmentData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AppointmentDataRepository extends PagingAndSortingRepository<AppointmentData, String> {

    Page<AppointmentData> findBySeasonAndTerritorialId(String season, String territorialId, Pageable pageable);

}
