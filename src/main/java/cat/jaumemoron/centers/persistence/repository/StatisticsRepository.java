package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.constants.StatisticType;
import cat.jaumemoron.centers.persistence.domain.Statistic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface StatisticsRepository extends PagingAndSortingRepository<Statistic, Date> {

    Page<Statistic> findByType(StatisticType type, Pageable pageable);

    Page<Statistic> findByTypeAndDateBetween(StatisticType type, Date init, Date end, Pageable pageable);

    void deleteByType(StatisticType type);
}
