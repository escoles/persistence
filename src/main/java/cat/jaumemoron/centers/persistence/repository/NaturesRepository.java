package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Nature;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface NaturesRepository extends PagingAndSortingRepository<Nature, String> {

    List<Nature> findAll(Sort sort);

}
