package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Action;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ActionsRepository extends PagingAndSortingRepository<Action, String> {

    Page<Action> findAll(Pageable pageable);

    Page<Action> findByTextContainingIgnoreCase(String text, Pageable pageable);

    long countByRead(boolean read);

}
