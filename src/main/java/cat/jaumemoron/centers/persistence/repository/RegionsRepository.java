package cat.jaumemoron.centers.persistence.repository;

import cat.jaumemoron.centers.persistence.domain.Region;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RegionsRepository extends PagingAndSortingRepository<Region, String> {

    List<Region> findAll(Sort sort);


}
