package cat.jaumemoron.centers.persistence.dto;

import cat.jaumemoron.centers.persistence.domain.AppointmentTerritorial;

public class TerritorialCountDTO {

    private AppointmentTerritorial territorial;
    private long count;

    public AppointmentTerritorial getTerritorial() {
        return territorial;
    }

    public long getCount() {
        return count;
    }


    public static final class Builder {
        private AppointmentTerritorial territorial;
        private long count;

        private Builder() {
        }

        public static Builder aTerritorialCountDTO() {
            return new Builder();
        }

        public Builder withTerritorial(AppointmentTerritorial territorial) {
            this.territorial = territorial;
            return this;
        }

        public Builder withCount(long count) {
            this.count = count;
            return this;
        }

        public TerritorialCountDTO build() {
            TerritorialCountDTO territorialCountDTO = new TerritorialCountDTO();
            territorialCountDTO.territorial = this.territorial;
            territorialCountDTO.count = this.count;
            return territorialCountDTO;
        }
    }
}
