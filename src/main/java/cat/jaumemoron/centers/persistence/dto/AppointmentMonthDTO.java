package cat.jaumemoron.centers.persistence.dto;

import static cat.jaumemoron.centers.persistence.constants.PersistenceConstants.INIT_SEASON_MONTH_AS_NUMBER;

public class AppointmentMonthDTO implements Comparable<AppointmentMonthDTO> {

    private Integer month;
    private long total;

    AppointmentMonthDTO(Integer month, long total) {
        this.month = month;
        this.total = total;
    }

    public static AppointmentMonthDTO of(Integer month, long total) {
        return new AppointmentMonthDTO(month, total);
    }

    public Integer getMonth() {
        return month;
    }

    public long getTotal() {
        return total;
    }

    @Override
    public int compareTo(AppointmentMonthDTO o) {
        if ((this.month >= INIT_SEASON_MONTH_AS_NUMBER && o.month >= INIT_SEASON_MONTH_AS_NUMBER) ||
                (this.month < INIT_SEASON_MONTH_AS_NUMBER && o.month < INIT_SEASON_MONTH_AS_NUMBER)) {
            return this.month.compareTo(o.month);
        } else if (this.month < INIT_SEASON_MONTH_AS_NUMBER)
            return 1;
        else
            return -1;
    }
}
