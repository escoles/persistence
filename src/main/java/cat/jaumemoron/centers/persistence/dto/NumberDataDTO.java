package cat.jaumemoron.centers.persistence.dto;

import cat.jaumemoron.centers.persistence.domain.Appointment;

import java.util.ArrayList;
import java.util.List;

public class NumberDataDTO {

    private String season;
    private List<Appointment> appointmentList = new ArrayList<Appointment>();
    private boolean found;

    public NumberDataDTO(String season, List<Appointment> appointmentList, boolean found) {
        this.season = season;
        this.appointmentList = appointmentList;
        this.found = found;
    }

    public List<Appointment> getAppointmentList() {
        return appointmentList;
    }

    public boolean isFound() {
        return found;
    }

    public String getSeason() {
        return season;
    }
}
