package cat.jaumemoron.centers.persistence.dto;

import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class AppointmentDTO {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentDTO.class);

    private SimpleDateFormat source = new SimpleDateFormat(PersistenceConstants.DEFAULT_DATE_FORMAT);
    private SimpleDateFormat target = new SimpleDateFormat(PersistenceConstants.CSV_DATE_FORMAT);

    private String territorial = "";
    private String date = "";
    private String number = "";
    private String center = "";
    private String dayType = "";
    private String speciality = "";
    private String init = "";
    private String end = "";

    public String getTerritorial() {
        return territorial;
    }

    public void setTerritorial(String territorial) {
        this.territorial = territorial;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getDayType() {
        return dayType;
    }

    public void setDayType(String dayType) {
        this.dayType = dayType;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getInit() {
        return init;
    }

    public void setInit(String init) {
        this.init = init;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        String sDate = "";
        String sInit = "";
        String sEnd = "";
        if (!ObjectUtils.isEmpty(date)) {
            try {
                sDate = target.format(source.parse(date));
            } catch (ParseException e) {
                LOGGER.warn("Error with appointment date parser: " + e.getMessage());
            }
        }
        if (!ObjectUtils.isEmpty(init)) {
            try {
                sInit = target.format(source.parse(init));
            } catch (ParseException e) {
                LOGGER.warn("Error with appointment date parser: " + e.getMessage());
            }
        }
        if (!ObjectUtils.isEmpty(end)) {
            try {
                sEnd = target.format(source.parse(end));
            } catch (ParseException e) {
                LOGGER.warn("Error with appointment date parser: " + e.getMessage());
            }
        }


        return territorial + ',' +
                sDate + ',' +
                number + ',' +
                "\"" + center + "\"," +
                "\"" + dayType + "\"," +
                speciality + ',' +
                sInit + ',' +
                sEnd;
    }
}
