package cat.jaumemoron.centers.persistence.dto;

import cat.jaumemoron.centers.persistence.constants.DayType;

public class DayTypeDTO implements Comparable<DayTypeDTO> {

    private DayType type;
    private long total;

    DayTypeDTO(DayType type, long total) {
        this.type = type;
        this.total = total;
    }

    public static DayTypeDTO of(DayType type, long total) {
        return new DayTypeDTO(type, total);
    }

    public DayType getType() {
        return type;
    }

    public long getTotal() {
        return total;
    }

    @Override
    public int compareTo(DayTypeDTO o) {
        return this.type.compareTo(o.type);
    }
}
