package cat.jaumemoron.centers.persistence.exception;

import java.util.ArrayList;
import java.util.List;

public class DataRequiredException extends RuntimeException {

    private List<String> errorList = new ArrayList<>();

    public DataRequiredException(String message, List<String> errorList) {
        super(message);
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}
