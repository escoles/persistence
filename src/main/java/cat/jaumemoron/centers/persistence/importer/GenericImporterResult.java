package cat.jaumemoron.centers.persistence.importer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GenericImporterResult {

    private File file;
    private long recordsNumber;
    private long initTime;
    private long endTime;
    private boolean hasErrors;
    private List<String> errors = new ArrayList<>();

    protected GenericImporterResult(File file) {
        this.file = file;
        initTime = System.currentTimeMillis();
    }

    public File getFile() {
        return file;
    }

    public long getRecordsNumber() {
        return recordsNumber;
    }

    public long getProcessTime() {
        if (endTime > 0)
            return endTime - initTime;
        else
            return System.currentTimeMillis() - initTime;
    }

    public void finish() {
        endTime = System.currentTimeMillis();
    }

    public boolean hasErrors() {
        return hasErrors;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void addError(String error) {
        this.errors.add(error);
        hasErrors = true;
    }

    public void incrementRecordsNumber() {
        recordsNumber++;
    }

    public String getFileName() {
        return file.getName();
    }

}
