package cat.jaumemoron.centers.persistence.importer;

import cat.jaumemoron.centers.persistence.domain.Territorial;
import cat.jaumemoron.centers.persistence.repository.TerritorialsRepository;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Optional;

@Component
public class TerritorialFileImporter extends GenericImporter {

    @Autowired
    private TerritorialsRepository repository;

    @Override
    protected GenericImporterResult getImporterResult(File file) {
        return new GenericImporterResult(file);
    }

    protected void processRecord(CSVRecord record) throws Exception {
        String id = record.get("id");
        String name = record.get("name");
        String appointmentId = record.get("appointmentId");
        if (id == null || id.isEmpty())
            throw new IllegalArgumentException("Id cannot be null");
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Name cannot be null");
        if (appointmentId == null || appointmentId.isEmpty())
            throw new IllegalArgumentException("Appointment Id cannot be null");
        Territorial territorial;
        Optional<Territorial> optional = repository.findById(id);
        if (optional.isPresent()) {
            territorial = optional.get();
            territorial.setName(name);
            territorial.setAppointmentId(appointmentId);
        } else
            territorial = Territorial.of(id, name, appointmentId);
        repository.save(territorial);
    }

    protected void updateSystem() {
        // Do nothing
    }
}
