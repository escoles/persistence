package cat.jaumemoron.centers.persistence.importer;

import cat.jaumemoron.centers.persistence.domain.*;
import cat.jaumemoron.centers.persistence.exception.DataRequiredException;
import cat.jaumemoron.centers.persistence.repository.CitiesRepository;
import cat.jaumemoron.centers.persistence.repository.NaturesRepository;
import cat.jaumemoron.centers.persistence.repository.OwnersRepository;
import cat.jaumemoron.centers.persistence.repository.RegionsRepository;
import cat.jaumemoron.centers.persistence.service.CenterService;
import cat.jaumemoron.centers.persistence.service.TerritorialService;
import cat.jaumemoron.centers.persistence.service.TypeService;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.NumberFormat;
import java.util.*;

@Component
public class CenterFileImporter extends GenericImporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CenterFileImporter.class);

    @Autowired
    private CenterService service;

    @Autowired
    private CitiesRepository citiesRepository;

    @Autowired
    private RegionsRepository regionsRepository;

    @Autowired
    private TerritorialService territorialService;

    @Autowired
    private NaturesRepository naturesRepository;

    @Autowired
    private OwnersRepository ownersRepository;

    @Autowired
    private TypeService typeService;

    @Override
    protected GenericImporterResult getImporterResult(File file) {
        return new CenterImporterResult(file);
    }

    protected void processRecord(CSVRecord record) throws Exception {
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
        try {
            String id = record.get("Codi centre");
            String name = record.get("Denominació completa");
            String address = record.get("Adreça");
            String postalCode = record.get("Codi postal");
            String codiMunicipi = record.get("Codi municipi");
            String municipi = record.get("Nom municipi");
            String codiRegion = record.get("Codi comarca");
            String region = record.get("Nom comarca");
            String phone = record.get("Telèfon");
            String email = record.get("E-mail centre");
            String fax = record.get("FAX");
            Number latitude = 0;
            Number longitude = 0;
            String sLatitude = record.get("Coordenades GEO Y");
            String sLongitude = record.get("Coordenades GEO X");
            if (sLatitude != null && !sLatitude.isEmpty())
                latitude = format.parse(record.get("Coordenades GEO Y"));
            if (sLongitude != null && !sLongitude.isEmpty())
                longitude = format.parse(record.get("Coordenades GEO X"));
            String codiTerritorial = record.get("Codi delegació");
            String nomTerritorial = record.get("Nom delegació");
            String nature = record.get("Nom naturalesa");
            String codiNature = record.get("Codi naturalesa");
            String codiOwner = record.get("Codi titularitat");
            String owner = record.get("Nom titularitat");
            String type = record.get("Nivells educatius");
            Center center = Center.Builder.aCenter()
                    .withId(id)
                    .withName(name)
                    .withAddress(address)
                    .withPostalCode(postalCode)
                    .withCity(City.of(codiMunicipi, municipi))
                    .withRegion(Region.of(codiRegion, region))
                    .withPhone(phone)
                    .withEmail(email)
                    .withFax(fax)
                    .withLocation(latitude.doubleValue(), longitude.doubleValue())
                    .withTerritorial(getTerritorial(codiTerritorial, nomTerritorial))
                    .withNature(Nature.of(codiNature, nature))
                    .withOwner(Owner.of(codiOwner, owner)).build();
            center.setLastModificationDate(new Date());
            StringTokenizer st = new StringTokenizer(type);
            while (st.hasMoreTokens()) {
                String sType = st.nextToken();
                if (sType != null && !sType.isEmpty())
                    center.addType(getType(sType));

            }
            service.save(center);
            // Desem les entitats que hem crear/actualitzar
            if (center.getCity().getId() != null && !center.getCity().getId().isEmpty())
                ((CenterImporterResult) getResult()).addCity(center.getCity());
            if (center.getRegion().getId() != null && !center.getRegion().getId().isEmpty())
                ((CenterImporterResult) getResult()).addRegion(center.getRegion());
            if (center.getNature().getId() != null && !center.getNature().getId().isEmpty())
                ((CenterImporterResult) getResult()).addNature(center.getNature());
            if (center.getOwner().getId() != null && !center.getOwner().getId().isEmpty())
                ((CenterImporterResult) getResult()).addOwner(center.getOwner());
        } catch (DataRequiredException e) {
            for (String error : e.getErrorList()) {
                traceError(error, e);
            }
        }
    }

    protected void updateSystem() {
        if (!((CenterImporterResult) getResult()).getCityList().isEmpty())
            citiesRepository.saveAll(((CenterImporterResult) getResult()).getCityList());
        if (!((CenterImporterResult) getResult()).getRegionList().isEmpty())
            regionsRepository.saveAll(((CenterImporterResult) getResult()).getRegionList());
        if (!((CenterImporterResult) getResult()).getNatureList().isEmpty())
            naturesRepository.saveAll(((CenterImporterResult) getResult()).getNatureList());
        if (!((CenterImporterResult) getResult()).getOwnerList().isEmpty())
            ownersRepository.saveAll(((CenterImporterResult) getResult()).getOwnerList());
    }

    private Territorial getTerritorial(String codiTerritorial, String nomTerritorial) {
        List<Territorial> territorialList = territorialService.findAll();
        Territorial territorial = Territorial.of(codiTerritorial, nomTerritorial);
        int index = territorialList.indexOf(territorial);
        if (index < 0) {
            throw new DataRequiredException("Territorial [" + territorial + "] not found", Collections.emptyList());
        } else {
            return territorialList.get(index);
        }
    }

    private Type getType(String type) {
        Type centerType = Type.of(type, type);
        List<Type> typeList = typeService.findAll();
        int index = typeList.indexOf(centerType);
        if (index < 0) {
            LOGGER.debug("Type {} not found", centerType.getName());
            return centerType;
        } else
            return typeList.get(index);
    }
}
