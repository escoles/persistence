package cat.jaumemoron.centers.persistence.importer;

import cat.jaumemoron.centers.persistence.domain.City;
import cat.jaumemoron.centers.persistence.domain.Nature;
import cat.jaumemoron.centers.persistence.domain.Owner;
import cat.jaumemoron.centers.persistence.domain.Region;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class CenterImporterResult extends GenericImporterResult {

    private Set<City> cityList = new HashSet<>();
    private Set<Region> regionList = new HashSet<>();
    private Set<Nature> natureList = new HashSet<>();
    private Set<Owner> ownerList = new HashSet<>();

    protected CenterImporterResult(File file) {
        super(file);
    }


    public Set<City> getCityList() {
        return cityList;
    }

    public Set<Region> getRegionList() {
        return regionList;
    }

    public Set<Nature> getNatureList() {
        return natureList;
    }

    public Set<Owner> getOwnerList() {
        return ownerList;
    }

    public void addCity(City city) {
        cityList.add(city);
    }

    public void addRegion(Region region) {
        regionList.add(region);
    }

    public void addNature(Nature nature) {
        natureList.add(nature);
    }

    public void addOwner(Owner owner) {
        ownerList.add(owner);
    }
}
