package cat.jaumemoron.centers.persistence.importer;

import cat.jaumemoron.centers.persistence.event.EventPublisher;
import cat.jaumemoron.centers.persistence.event.ImporterEvent;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.elasticsearch.index.mapper.MapperParsingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class GenericImporter extends EventPublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenericImporter.class);

    private GenericImporterResult result;

    protected GenericImporterResult getResult() {
        return result;
    }

    public GenericImporterResult importFile(String filename) throws IOException {
        return importFile(filename, true);
    }

    public GenericImporterResult importFile(String filename, boolean publishResult) throws IOException {
        if (filename == null)
            throw new IllegalArgumentException("Filename cannot be null");
        File file = new File(filename);
        validateFile(file);
        this.result = getImporterResult(file);
        try (Reader in = new FileReader(file)) {
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                result.incrementRecordsNumber();
                try {
                    processRecord(record);
                } catch (IllegalArgumentException e) {
                    traceError("Not enough values: " + e.getMessage(), e);
                } catch (MapperParsingException e) {
                    String message = e.getMessage();
                    if (e.getCause() != null && e.getCause().getMessage() != null)
                        message = e.getCause().getMessage();
                    message = message + "]";
                    traceError(message, e);
                } catch (Exception e) {
                    traceError("Error creating entity: " + e.getMessage(), e);
                }
            }
            // Actualitzem les entitats si cal
            updateSystem();
            // Llancem event de procés finalitzat si cal
            if (publishResult)
                publish(new ImporterEvent(result));
        } catch (IOException e) {
            traceError("Error reading csv file: " + e.getMessage(), e);
        } catch (Exception e) {
            traceError("Unknown error: " + e.getMessage(), e);
        } finally {
            // Indiquem que el procés ha finalitzat
            result.finish();
            LOGGER.info("File [{}] processed successfully. Processed: {} records taking {} millis"
                    , result.getFileName(), result.getRecordsNumber(), result.getProcessTime());
            writeErrorsOnLog();
        }
        return result;
    }

    private void validateFile(File file) {
        if (!file.exists())
            throw new IllegalArgumentException("File [" + file + "] does not exists");
        if (file.isDirectory())
            throw new IllegalArgumentException("Invalid file. File [" + file + "] is a directory");
    }

    private void writeErrorsOnLog() {
        // Si s'han produït errors els escribim al log
        if (result.hasErrors()) {
            LOGGER.info("Found {} errors", result.getErrors().size());
            LOGGER.info("Error list");
            for (String error : result.getErrors()) {
                LOGGER.info(error);
            }
        }
    }

    protected abstract GenericImporterResult getImporterResult(File file);

    protected void traceError(String message, Exception e) {
        LOGGER.debug(message, e);
        result.addError("[" + result.getRecordsNumber() + "] " + message);
    }

    protected abstract void processRecord(CSVRecord record) throws Exception;

    protected abstract void updateSystem();

    protected Date getDate(String date, String format) {
        if (date != null && !date.isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            try {
                return sdf.parse(date);
            } catch (ParseException e) {
                String message = "Error on date value [" + date + "]";
                traceError(message, e);
            }
        }
        return null;
    }
}
