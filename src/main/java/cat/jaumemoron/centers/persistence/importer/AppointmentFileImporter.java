package cat.jaumemoron.centers.persistence.importer;

import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.domain.Appointment;
import cat.jaumemoron.centers.persistence.domain.Season;
import cat.jaumemoron.centers.persistence.domain.Speciality;
import cat.jaumemoron.centers.persistence.domain.Territorial;
import cat.jaumemoron.centers.persistence.service.AppointmentService;
import cat.jaumemoron.centers.persistence.service.SeasonsService;
import cat.jaumemoron.centers.persistence.service.SpecialitiesService;
import cat.jaumemoron.centers.persistence.service.TerritorialService;
import cat.jaumemoron.centers.persistence.utils.PersistenceUtils;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.io.File;
import java.util.Optional;

@Component
public class AppointmentFileImporter extends GenericImporter {

    @Autowired
    private AppointmentService service;

    @Autowired
    private TerritorialService territorialService;

    @Autowired
    private SpecialitiesService specialitiesService;

    @Autowired
    private SeasonsService seasonsService;

    @Override
    protected GenericImporterResult getImporterResult(File file) {
        return new GenericImporterResult(file);
    }

    protected void processRecord(CSVRecord record) throws Exception {
        String territorialId = record.get(PersistenceConstants.CSV_TERRITORIAL_FIELD);
        String sDate = record.get(PersistenceConstants.CSV_DATE_FIELD);
        String number = record.get(PersistenceConstants.CSV_NUMBER_FIELD);
        String centerName = record.get(PersistenceConstants.CSV_CENTER_FIELD);
        String dayType = record.get(PersistenceConstants.CSV_DAYTYPE_FIELD);
        String specialityId = record.get(PersistenceConstants.CSV_SPECIALITY_FIELD);
        String sInit = record.get(PersistenceConstants.CSV_INI_DATE_FIELD);
        String sEnd = record.get(PersistenceConstants.CSV_END_DATE_FIELD);
        if (territorialId == null || territorialId.isEmpty())
            throw new IllegalArgumentException("Territorial cannot be null");
        if (sDate == null || sDate.isEmpty())
            throw new IllegalArgumentException("Date cannot be null");
        if (number == null || number.isEmpty())
            throw new IllegalArgumentException("Number cannot be null");
        if (centerName == null || centerName.isEmpty())
            throw new IllegalArgumentException("Center cannot be null");
        if (specialityId == null || specialityId.isEmpty())
            throw new IllegalArgumentException("Speciality cannot be null");
        long n = Long.parseLong(number);
        if (n > 250000)
            throw new IllegalArgumentException("Appointment number has an invalid value [" + n + "]");
        Appointment appointment = new Appointment();
        appointment.setNumber(n);
        // Convertim les dades
        appointment.setDate(getDate(sDate, PersistenceConstants.CSV_DATE_FORMAT));
        appointment.setInit(getDate(sInit, PersistenceConstants.CSV_DATE_FORMAT));
        appointment.setEnd(getDate(sEnd, PersistenceConstants.CSV_DATE_FORMAT));

        if (!ObjectUtils.isEmpty(dayType)) {
            appointment.setDayType(DayType.get(dayType));
            if (appointment.getDayType().equals(DayType.UNKNOWN)) {
                traceError("Day type [" + dayType + "] is a unknown value", null);
            }
        } else {
            appointment.setDayType(DayType.UNKNOWN);
            traceError("Day type has not a value", null);
        }
        // Busquem l'especialitat
        Optional<Speciality> sOptional = specialitiesService.findById(specialityId);
        if (sOptional.isPresent()) {
            appointment.setSpeciality(sOptional.get());
        } else
            throw new IllegalArgumentException("Speciality [" + specialityId + "] does not exists");
        // Busquem la territorial
        Optional<Territorial> tOptional = territorialService.findByAppointmentId(territorialId);
        if (tOptional.isPresent())
            appointment.setTerritorial(tOptional.get());
        else
            throw new IllegalArgumentException("Territorial [" + territorialId + "] does not exists");
        // Assignem el nom del centre original
        appointment.setCenterName(centerName);
        String seasonId = PersistenceUtils.getSeason(appointment.getDate());
        // Mirem si el curs existeix. Sino, el crearem
        Optional<Season> optional = seasonsService.findById(seasonId);
        Season season;
        if (optional.isPresent())
            season = optional.get();
        else {
            season = Season.of(seasonId);
            season = seasonsService.create(season);
        }
        appointment.setSeason(season);
        // Desem les dades del nomenament
        service.save(appointment);
    }

    protected void updateSystem() {
        // Do nothing
    }


}
