package cat.jaumemoron.centers.persistence.importer;


import cat.jaumemoron.centers.persistence.domain.Speciality;
import cat.jaumemoron.centers.persistence.repository.SpecialitiesRepository;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Optional;

@Component
public class SpecialityFileImporter extends GenericImporter {

    @Autowired
    private SpecialitiesRepository repository;

    @Override
    protected GenericImporterResult getImporterResult(File file) {
        return new GenericImporterResult(file);
    }

    protected void processRecord(CSVRecord record) throws Exception {
        String id = record.get("id");
        String name = record.get("name");
        if (id == null || id.isEmpty())
            throw new IllegalArgumentException("Id cannot be null");
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Name cannot be null");
        Speciality speciality;
        Optional<Speciality> optional = repository.findById(id);
        if (optional.isPresent()) {
            speciality = optional.get();
            speciality.setName(name);
        } else
            speciality = Speciality.of(id, name);
        repository.save(speciality);
    }

    protected void updateSystem() {
        // Do nothing
    }
}