package cat.jaumemoron.centers.persistence.importer;

import cat.jaumemoron.centers.persistence.domain.Type;
import cat.jaumemoron.centers.persistence.repository.TypesRepository;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Optional;

@Component
public class TypeFileImporter extends GenericImporter {

    @Autowired
    private TypesRepository repository;

    @Override
    protected GenericImporterResult getImporterResult(File file) {
        return new GenericImporterResult(file);
    }

    protected void processRecord(CSVRecord record) throws Exception {
        String id = record.get("id");
        String name = record.get("name");
        if (id == null || id.isEmpty())
            throw new IllegalArgumentException("Id cannot be null");
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Name cannot be null");
        Type type;
        Optional<Type> optional = repository.findById(id);
        if (optional.isPresent()) {
            type = optional.get();
            type.setName(name);
        } else
            type = Type.of(id, name);
        repository.save(type);
    }

    protected void updateSystem() {
        // Do nothing
    }
}