package cat.jaumemoron.centers.persistence.utils;

import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PersistenceUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceUtils.class);

    public static String getSeason(Date date) {
        DateTime datetime = new DateTime(date);
        int year = datetime.getYear();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        Date endSeasonDate = null;
        try {
            endSeasonDate = sdf.parse(PersistenceConstants.INIT_SEASON_DAY + PersistenceConstants.INIT_SEASON_MONTH + year);
            if (endSeasonDate.before(date) || endSeasonDate.equals(date))
                return year + "-" + (year + 1);
            else
                return (year - 1) + "-" + year;
        } catch (ParseException e) {
            LOGGER.warn("Error getting season with date {}. Using current season", date);
            return DateTime.now().getYear() + "-" + DateTime.now().getYear();
        }
    }

    public static String getCurrentSeason() {
        return getSeason(new Date());
    }

    public static <T> List<T> toList(SearchHits<T> hits) {
        List<T> list = new ArrayList<>();
        for (SearchHit<T> hit : hits) {
            list.add(hit.getContent());
        }
        return list;
    }

    public static <T> Page<T> toPage(SearchHits<T> hits, Pageable pageable) {
        return new PageImpl<>(toList(hits), pageable, hits.getTotalHits());
    }
}
