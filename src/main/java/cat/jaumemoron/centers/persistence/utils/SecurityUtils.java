package cat.jaumemoron.centers.persistence.utils;

import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.ArrayList;
import java.util.List;

public class SecurityUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityUtils.class);

    public static void initializeSecurityContextAsAdmin() {
        initializeSecurityContext(PersistenceConstants.ADMIN_USERNAME, PersistenceConstants.ADMIN_USERNAME,
                PersistenceConstants.ROLE_ADMIN);
    }

    private static void initializeSecurityContext(String username, String password, String... roles) {
        List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
        if (roles != null) {
            for (String role : roles) {
                grantedAuths.add(new SimpleGrantedAuthority(role));
            }
        }
        User user = new User(username, password, grantedAuths);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, password, grantedAuths));

        LOGGER.debug("Setting user [{}] to Security Context with roles {}", user, roles);
    }

}
