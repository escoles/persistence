package cat.jaumemoron.centers.persistence.constants;

public class CacheConstants {

    public static final String REGIONS_CACHE_KEY = "REGIONS";
    public static final String CITIES_CACHE_KEY = "CITIES";
    public static final String NATURES_CACHE_KEY = "NATURES";
    public static final String OWNERS_CACHE_KEY = "OWNERS";
    public static final String TYPES_CACHE_KEY = "TYPES";
    public static final String TERRITORIALS_CACHE_KEY = "TERRITORIALS";
    public static final String TERRITORIAL_ID_CACHE_KEY = "TERRITORIAL_ID";
    public static final String TERRITORIALS_APP_ID_CACHE_KEY = "TERRITORIAL_APP_ID";
    public static final String TOTAL_CENTERS_CACHE_KEY = "TOTAL_CENTERS";
    public static final String CENTER_TERRITORIAL_CACHE_KEY = "CENTER_TERRITORIAL";
    public static final String SPECIALITIES_CACHE_KEY = "SPECIALITIES";
    public static final String SPECIALITIES_CACHE_NAME_KEY = "SPECIALITIES_NAME";
    public static final String SEASONS_CACHE_KEY = "SEASONS";
    public static final String SEASONS_CACHE_ID_KEY = "SEASONS_ID";
    public static final String TOTAL_APPOINTMENTS_BY_TERRITORIAL = "TOTAL_APPOINTMENTS_BY_TERRITORIAL";

    public static final String STATISTICS_TYPE_CACHE_KEY = "STATISTICS_TYPE";
    public static final String STATISTICS_TYPE_DATE_CACHE_KEY = "STATISTICS_TYPE_DATE_CACHE_KEY";
}
