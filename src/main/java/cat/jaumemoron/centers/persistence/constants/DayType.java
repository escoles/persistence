package cat.jaumemoron.centers.persistence.constants;

public enum DayType {

    UNKNOWN("0"),
    A_THIRD("33"),
    HALF("5"),
    TWO_THIRDS("66"),
    TREE_QUARTER("83"),
    FULL("1");

    private String value;

    DayType(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    public static DayType get(String value) {
        if (value != null) {
            if (A_THIRD.value.equals(value) || value.contains(A_THIRD.value))
                return A_THIRD;
            else if (HALF.value.equals(value) || value.contains(HALF.value))
                return HALF;
            else if (TWO_THIRDS.value.equals(value) || value.contains(TWO_THIRDS.value))
                return TWO_THIRDS;
            else if (TREE_QUARTER.value.equals(value) || value.contains(TREE_QUARTER.value))
                return TREE_QUARTER;
            else if (FULL.value.equals(value) || value.contains(FULL.value))
                return FULL;
        }
        return UNKNOWN;
    }
}
