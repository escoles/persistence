package cat.jaumemoron.centers.persistence.constants;

public class PersistenceConstants {

    public static final String CSV_TERRITORIAL_FIELD = "territorial";
    public static final String CSV_DATE_FIELD = "date";
    public static final String CSV_NUMBER_FIELD = "number";
    public static final String CSV_CENTER_FIELD = "center";
    public static final String CSV_DAYTYPE_FIELD = "dayType";
    public static final String CSV_SPECIALITY_FIELD = "speciality";
    public static final String CSV_INI_DATE_FIELD = "init";
    public static final String CSV_END_DATE_FIELD = "end";

    public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    public static final String CSV_DATE_FORMAT = "yyyy-MM-dd";

    public static final String ROLE_PREFIX = "ROLE_";
    public static final String ADMIN = "ADMIN";
    public static final String ROLE_ADMIN = ROLE_PREFIX + ADMIN;

    public static final String ADMIN_USERNAME = "admin";

    public static final String INIT_SEASON_DAY = "01";
    public static final String INIT_SEASON_MONTH = "07";
    public static final Integer INIT_SEASON_MONTH_AS_NUMBER = Integer.valueOf(INIT_SEASON_MONTH);
}
