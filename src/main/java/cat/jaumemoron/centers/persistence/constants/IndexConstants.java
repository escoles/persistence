package cat.jaumemoron.centers.persistence.constants;

public class IndexConstants {

    public static final String CENTERS_NAME = "center";
    public static final String CENTERS_TYPE = "center";

    public static final String DICTIONARY_NAME = "dictionary";
    public static final String REGIONS_TYPE = "region";
    public static final String CITIES_TYPE = "city";
    public static final String NATURE_TYPE = "nature";
    public static final String OWNER_TYPE = "owner";
    public static final String TYPES_YPE = "type";

    public static final String ACTIONS_NAME = "action";
    public static final String ACTIONS_TYPE = "action";

    public static final String APPOINTMENT_DATA_NAME = "appointment-data";
    public static final String APPOINTMENT_DATA_TYPE = "appointment-data";
    public static final String APPOINTMENT_DATA_DAYTYPE_NAME = "appointment-data-daytype";
    public static final String APPOINTMENT_DATA_DAYTYPE_TYPE = "appointment-data-daytype";


}
