package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.City;

import java.util.List;

public interface CityService {

    List<City> findAll();

}
