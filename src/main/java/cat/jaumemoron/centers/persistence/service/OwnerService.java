package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Owner;

import java.util.List;

public interface OwnerService {

    List<Owner> findAll();

}
