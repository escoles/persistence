package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Appointment;
import cat.jaumemoron.centers.persistence.domain.AppointmentData;
import cat.jaumemoron.centers.persistence.domain.AppointmentDataDayType;
import cat.jaumemoron.centers.persistence.dto.NumberDataDTO;
import cat.jaumemoron.centers.persistence.dto.TerritorialCountDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AppointmentService {

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    Appointment save(Appointment appointment);

    Page<Appointment> findByTerritorial(String season, String territorialId, Pageable pageable);

    Page<Appointment> findByTerritorialAndSpeciality(String season, String territorialId, String specialityId, Pageable pageable);

    Optional<Appointment> findById(String appointmentId);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    List<AppointmentData> getMaxAppointmentNumber(String seasonId);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    List<AppointmentData> getMaxAppointmentNumberFromLastDate(String seasonId);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    List<AppointmentDataDayType> getAppointmentDataGroupByTerritorialAndSpecialityAndDayType(String seasonId);

    long countBySeasonId(String seasonId);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    List<TerritorialCountDTO> getTerritorialsCountByDate(Date date);

    List<TerritorialCountDTO> getTerritorialsLastNumber(String seasonId);

    NumberDataDTO getNumberData(String seasonId, String territorialId, String specialityId, long number);
}
