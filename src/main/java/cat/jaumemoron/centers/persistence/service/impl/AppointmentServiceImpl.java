package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.domain.*;
import cat.jaumemoron.centers.persistence.dto.NumberDataDTO;
import cat.jaumemoron.centers.persistence.dto.TerritorialCountDTO;
import cat.jaumemoron.centers.persistence.repository.AppointmentsRepository;
import cat.jaumemoron.centers.persistence.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentsRepository repository;

    @Override
    @CacheEvict(value = CacheConstants.TOTAL_APPOINTMENTS_BY_TERRITORIAL, allEntries = true)
    public Appointment save(Appointment appointment) {
        if (appointment == null)
            throw new IllegalArgumentException("Appointment cannot be null");
        if (ObjectUtils.isEmpty(appointment.getId())) {
            String id = "";
            if (appointment.getTerritorial() != null)
                id = appointment.getTerritorial().getId();
            long time = 0;
            if (appointment.getDate() != null)
                time = appointment.getDate().getTime();
            appointment.setId(id + appointment.getNumber() + time);

        }
        return repository.save(appointment);
    }

    @Override
    public Page<Appointment> findByTerritorial(String season, String territorialId, Pageable pageable) {
        if (ObjectUtils.isEmpty(season))
            throw new IllegalArgumentException("Season cannot be null");
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("TerritorialId cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findBySeasonIdAndTerritorialId(season, territorialId, pageable);
    }

    @Override
    public Page<Appointment> findByTerritorialAndSpeciality(String season, String territorialId, String specialityId, Pageable pageable) {
        if (ObjectUtils.isEmpty(season))
            throw new IllegalArgumentException("Season cannot be null");
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("TerritorialId cannot be null");
        if (ObjectUtils.isEmpty(specialityId))
            throw new IllegalArgumentException("Term cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findBySeasonIdAndTerritorialIdAndSpecialityId(season, territorialId, specialityId, pageable);
    }

    @Override
    public Optional<Appointment> findById(String appointmentId) {
        if (ObjectUtils.isEmpty(appointmentId))
            throw new IllegalArgumentException("Appointment id cannot be null");
        return repository.findById(appointmentId);
    }

    public List<AppointmentData> getMaxAppointmentNumber(String seasonId) {
        if (ObjectUtils.isEmpty(seasonId))
            throw new IllegalArgumentException("Season id cannot be null");
        return toAppointmentData(repository.getMaxAppointmentNumber(seasonId));
    }

    public List<AppointmentData> getMaxAppointmentNumberFromLastDate(String seasonId) {
        if (ObjectUtils.isEmpty(seasonId))
            throw new IllegalArgumentException("Season id cannot be null");
        return toAppointmentData(repository.getMaxAppointmentNumberFromLastDate(seasonId));
    }

    public List<AppointmentDataDayType> getAppointmentDataGroupByTerritorialAndSpecialityAndDayType(String seasonId) {
        if (ObjectUtils.isEmpty(seasonId))
            throw new IllegalArgumentException("Season id cannot be null");
        return toAppointmentDataDayType(repository.getAppointmentDataGroupByTerritorialAndSpecialityAndDayType(seasonId));
    }

    public List<TerritorialCountDTO> getTerritorialsCountByDate(Date date) {
        if (date == null)
            throw new IllegalArgumentException("Date cannot be null");
        return toTerritorialCountDTO(repository.getTerritorialsCountByDate(date));
    }

    public List<TerritorialCountDTO> getTerritorialsLastNumber(String seasonId) {
        if (ObjectUtils.isEmpty(seasonId))
            throw new IllegalArgumentException("Season id cannot be null");
        return toTerritorialCountDTO(repository.getTerritorialsLastNumber(seasonId));
    }

    private List<AppointmentData> toAppointmentData(List<Object[]> records) {
        List<AppointmentData> appointmentDataList = new ArrayList<>();
        for (Object[] record : records) {
            AppointmentData data = AppointmentData.Builder.anAppointmentData()
                    .withSeason(record[0].toString())
                    .withTerritorial(AppointmentTerritorial.of(record[1].toString(), record[2].toString()))
                    .withSpeciality(AppointmentSpeciality.of(record[3].toString(), record[4].toString()))
                    .withData(AppointmentNumberData.of(Long.parseLong(record[5].toString()), (Date) record[6])).build();
            data.setId(data.getSeason() + data.getTerritorial().getId()
                    + data.getSpeciality().getId()
                    + data.getData().getMaxDate().getTime()
                    + data.getData().getMaxNumber());

            data.setCount(repository.countBySeasonIdAndTerritorialIdAndSpecialityId(data.getSeason()
                    , data.getTerritorial().getId(), data.getSpeciality().getId()));
            appointmentDataList.add(data);
        }
        return appointmentDataList;
    }

    private List<AppointmentDataDayType> toAppointmentDataDayType(List<Object[]> records) {
        List<AppointmentDataDayType> appointmentDataList = new ArrayList<>();
        for (Object[] record : records) {
            AppointmentDataDayType data = AppointmentDataDayType.Builder.anAppointmentData()
                    .withSeason(record[0].toString())
                    .withTerritorial(AppointmentTerritorial.of(record[1].toString(), record[2].toString()))
                    .withSpeciality(AppointmentSpeciality.of(record[3].toString(), record[4].toString()))
                    .withData(AppointmentNumberData.of(Long.parseLong(record[5].toString()), (Date) record[6])).build();
            data.setDayType(DayType.values()[Integer.parseInt(record[7].toString())]);
            data.setId(data.getSeason() + data.getTerritorial().getId()
                    + data.getSpeciality().getId()
                    + data.getDayType()
                    + data.getData().getMaxDate().getTime()
                    + data.getData().getMaxNumber());
            appointmentDataList.add(data);
        }
        return appointmentDataList;
    }

    @Override
    @Cacheable(CacheConstants.TOTAL_APPOINTMENTS_BY_TERRITORIAL)
    public long countBySeasonId(String seasonId) {
        if (ObjectUtils.isEmpty(seasonId))
            throw new IllegalArgumentException("Season id cannot be null");
        return repository.countBySeasonId(seasonId);
    }

    private List<TerritorialCountDTO> toTerritorialCountDTO(List<Object[]> records) {
        List<TerritorialCountDTO> territorialCountDTOList = new ArrayList<>();
        for (Object[] record : records) {
            territorialCountDTOList.add(TerritorialCountDTO.Builder.aTerritorialCountDTO()
                    .withTerritorial(AppointmentTerritorial.of(record[0].toString(), record[1].toString()))
                    .withCount(Long.parseLong(record[2].toString())).build());
        }
        return territorialCountDTOList;
    }


    public NumberDataDTO getNumberData(String seasonId, String territorialId, String specialityId, long number) {
        if (ObjectUtils.isEmpty(seasonId))
            throw new IllegalArgumentException("Season id cannot be null");
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("Territorial id cannot be null");
        if (ObjectUtils.isEmpty(specialityId))
            throw new IllegalArgumentException("Speciality id cannot be null");
        if (number <= 0)
            throw new IllegalArgumentException("Number contains an invalid value");

        boolean found = true;
        List<Appointment> appointmentList = repository.findBySeasonIdAndTerritorialIdAndSpecialityIdAndNumber(seasonId, territorialId, specialityId, number);
        // Si no es troba busquem l'inmediatament anterior
        if (appointmentList.isEmpty()) {
            found = false;
            // Obtenim el número inferior més proper
            Appointment nearest = repository.findFirstBySeasonIdAndTerritorialIdAndSpecialityIdAndNumberLessThanOrderByNumberDesc(seasonId, territorialId, specialityId, number);
            // Si no s'en troba cap, obtenim el número superior més proper
            if (nearest == null)
                nearest = repository.findFirstBySeasonIdAndTerritorialIdAndSpecialityIdAndNumberGreaterThanOrderByNumberAsc(seasonId, territorialId, specialityId, number);
            if (nearest != null)
                appointmentList = repository.findBySeasonIdAndTerritorialIdAndSpecialityIdAndNumber(seasonId, territorialId, specialityId, nearest.getNumber());
        }
        return new NumberDataDTO(seasonId, appointmentList, found);
    }
}
