package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.domain.Action;
import cat.jaumemoron.centers.persistence.repository.ActionsRepository;
import cat.jaumemoron.centers.persistence.service.ActionService;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Optional;

import static cat.jaumemoron.centers.persistence.utils.PersistenceUtils.toPage;
import static org.elasticsearch.index.query.QueryBuilders.matchPhrasePrefixQuery;
import static org.elasticsearch.index.query.QueryBuilders.wildcardQuery;

@Service
public class ActionServiceImpl implements ActionService {

    @Autowired
    private ActionsRepository repository;

    @Autowired
    private ElasticsearchOperations operations;


    @Override
    public Page<Action> findAll(Pageable pageable) {
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findAll(pageable);
    }

    @Override
    public long countUnRead() {
        return repository.countByRead(false);
    }

    @Override
    public Action create(Action action) {
        if (action == null)
            throw new IllegalArgumentException("Action object cannot be null");
        return repository.save(action);
    }

    @Override
    public void markAsRead(String id) {
        if (ObjectUtils.isEmpty(id))
            throw new IllegalArgumentException("Id cannot be null");
        Optional<Action> optional = repository.findById(id);
        if (optional.isPresent()) {
            Action action = optional.get();
            action.setRead(true);
            repository.save(action);
        }
    }

    @Override
    public void delete(String id) {
        if (id == null || id.isEmpty())
            throw new IllegalArgumentException("Id cannot be null");
        repository.deleteById(id);
    }

    @Override
    public Optional<Action> findById(String id) {
        if (id == null || id.isEmpty())
            throw new IllegalArgumentException("Id cannot be null");
        return repository.findById(id);
    }

    @Override
    public Page<Action> findByTerm(String term, Pageable pageable) {
        String wildcardText = "*" + term + "*";
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        if (ObjectUtils.isEmpty(term))
            throw new IllegalArgumentException("Term cannot be empty");

        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .should(matchPhrasePrefixQuery("text", term))
                        .should(wildcardQuery("text", wildcardText)))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, Action.class), pageable);
    }

}
