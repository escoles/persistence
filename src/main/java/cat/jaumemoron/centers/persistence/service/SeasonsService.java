package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Season;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Optional;

public interface SeasonsService {

    List<Season> findAll();

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    Season create(Season season);

    Optional<Season> findById(String id);

}
