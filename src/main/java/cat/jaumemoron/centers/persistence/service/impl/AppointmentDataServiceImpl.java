package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.domain.AppointmentData;
import cat.jaumemoron.centers.persistence.domain.AppointmentDataDayType;
import cat.jaumemoron.centers.persistence.domain.Season;
import cat.jaumemoron.centers.persistence.repository.AppointmentDataDayTypeRepository;
import cat.jaumemoron.centers.persistence.repository.AppointmentDataRepository;
import cat.jaumemoron.centers.persistence.service.AppointmentDataService;
import cat.jaumemoron.centers.persistence.service.AppointmentService;
import cat.jaumemoron.centers.persistence.service.SeasonsService;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

import static cat.jaumemoron.centers.persistence.utils.PersistenceUtils.toPage;
import static org.elasticsearch.index.query.Operator.AND;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Service
public class AppointmentDataServiceImpl implements AppointmentDataService {


    private static final Logger LOGGER = LoggerFactory.getLogger(AppointmentDataServiceImpl.class);

    @Autowired
    private AppointmentDataRepository appointmentDataRepository;

    @Autowired
    private AppointmentDataDayTypeRepository appointmentDataDayTypeRepository;

    @Autowired
    private ElasticsearchOperations operations;
    @Autowired
    private AppointmentService appointmentService;

    @Autowired
    private SeasonsService seasonsService;

    @Override
    public Page<AppointmentData> findBySeasonAndTerritorial(String season, String territorialId, Pageable pageable) {
        if (ObjectUtils.isEmpty(season))
            throw new IllegalArgumentException("Season cannot be null");
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("Territorial Id cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable object cannot be null");
        return appointmentDataRepository.findBySeasonAndTerritorialId(season, territorialId, pageable);
    }

    @Override
    public Page<AppointmentDataDayType> findBySeasonAndTerritorialIdAndDayType(String season, String territorialId, DayType dayType, Pageable pageable) {
        if (ObjectUtils.isEmpty(season))
            throw new IllegalArgumentException("Season cannot be null");
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("Territorial Id cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable object cannot be null");
        return appointmentDataDayTypeRepository.findBySeasonAndTerritorialIdAndDayType(season, territorialId, dayType, pageable);
    }

    @Override
    public Page<AppointmentDataDayType> findBySeasonAndTerritorialIdAndDayTypeAll(String season, String territorialId, Pageable pageable) {
        if (ObjectUtils.isEmpty(season))
            throw new IllegalArgumentException("Season cannot be null");
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("Territorial Id cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable object cannot be null");
        return appointmentDataDayTypeRepository.findBySeasonAndTerritorialId(season, territorialId, pageable);
    }

    @Override
    public Page<AppointmentData> findBySeasonAndTerritorialAndSpeciality(String season, String territorialId, String specialityId, Pageable pageable) {
        if (ObjectUtils.isEmpty(season))
            throw new IllegalArgumentException("Season cannot be null");
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("Territorial Id cannot be null");
        if (ObjectUtils.isEmpty(specialityId))
            throw new IllegalArgumentException("Speciality cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable object cannot be null");
        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(matchQuery("season", season).operator(AND))
                        .must(matchQuery("territorial.id", territorialId).operator(AND))
                        .must(matchQuery("speciality.id", specialityId).operator(AND)))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, AppointmentData.class), pageable);
    }

    @Override
    public Page<AppointmentDataDayType> findBySeasonAndTerritorialAndSpecialityAndDayType(String season, String territorialId, String specialityId, DayType dayType, Pageable pageable) {
        if (ObjectUtils.isEmpty(season))
            throw new IllegalArgumentException("Season cannot be null");
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("Territorial Id cannot be null");
        if (ObjectUtils.isEmpty(specialityId))
            throw new IllegalArgumentException("Speciality cannot be null");
        if (dayType == null)
            throw new IllegalArgumentException("DayType cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable object cannot be null");
        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(matchQuery("season", season).operator(AND))
                        .must(matchQuery("territorial.id", territorialId).operator(AND))
                        .must(matchQuery("dayType", dayType.toString()).operator(AND))
                        .must(matchQuery("speciality.id", specialityId).operator(AND)))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, AppointmentDataDayType.class), pageable);
    }

    @Override
    public Page<AppointmentDataDayType> findBySeasonAndTerritorialAndSpecialityAllDayType(String season, String territorialId, String specialityId, Pageable pageable) {
        if (ObjectUtils.isEmpty(season))
            throw new IllegalArgumentException("Season cannot be null");
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("Territorial Id cannot be null");
        if (ObjectUtils.isEmpty(specialityId))
            throw new IllegalArgumentException("Speciality cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable object cannot be null");
        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(matchQuery("season", season).operator(AND))
                        .must(matchQuery("territorial.id", territorialId).operator(AND))
                        .must(matchQuery("speciality.id", specialityId).operator(AND)))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, AppointmentDataDayType.class), pageable);
    }

    @Override
    public void update() {
        // Recreem els index
        appointmentDataRepository.deleteAll();
        appointmentDataDayTypeRepository.deleteAll();
        // Obtenim els cursos
        List<Season> seasonList = seasonsService.findAll();
        // Per a cada curs, recalculem
        for (Season season : seasonList) {
            updateSeason(season);
        }
    }


    private void updateSeason(Season season) {
        long init = System.currentTimeMillis();
        LOGGER.info("Updating appointment data from season {}", season.getId());
        List<AppointmentData> appointmentDataList = appointmentService.getMaxAppointmentNumber(season.getId());
        // Obtenim el número ḿes alt de l'últim dia i actualitzem la llista anterior
        List<AppointmentData> maxList = appointmentService.getMaxAppointmentNumberFromLastDate(season.getId());
        for (AppointmentData data : maxList) {
            if (appointmentDataList.contains(data)) {
                int index = appointmentDataList.indexOf(data);
                appointmentDataList.get(index).getData().setLastNumber(data.getData().getMaxNumber());
                appointmentDataList.get(index).getData().setLastDate(data.getData().getMaxDate());
                // Agafem la última vegada que és present el nomenament (per si és repetit)
                index = appointmentDataList.lastIndexOf(data);
                if (index > 0) {
                    appointmentDataList.get(index).getData().setLastNumber(data.getData().getMaxNumber());
                    appointmentDataList.get(index).getData().setLastDate(data.getData().getMaxDate());
                }
            }
        }
        if (!appointmentDataList.isEmpty())
            appointmentDataRepository.saveAll(appointmentDataList);
        List<AppointmentDataDayType> appointmentDataDayTypeList = appointmentService.getAppointmentDataGroupByTerritorialAndSpecialityAndDayType(season.getId());
        if (!appointmentDataDayTypeList.isEmpty())
            appointmentDataDayTypeRepository.saveAll(appointmentDataDayTypeList);
        LOGGER.info("Season [{}] updated successfully taken {}", season.getId(), (System.currentTimeMillis() - init));
    }
}
