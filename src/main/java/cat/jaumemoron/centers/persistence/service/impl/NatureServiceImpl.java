package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.domain.Nature;
import cat.jaumemoron.centers.persistence.repository.NaturesRepository;
import cat.jaumemoron.centers.persistence.service.NatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NatureServiceImpl implements NatureService {

    @Autowired
    private NaturesRepository repository;

    @Override
    @Cacheable(CacheConstants.NATURES_CACHE_KEY)
    public List<Nature> findAll() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }
}
