package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Nature;

import java.util.List;

public interface NatureService {

    List<Nature> findAll();

}
