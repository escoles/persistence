package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.constants.StatisticType;
import cat.jaumemoron.centers.persistence.domain.Statistic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Date;
import java.util.Optional;

public interface StatisticsService {

    Optional<Statistic> findById(Date date);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    Page<Statistic> findByType(StatisticType type, Pageable pageable);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    Page<Statistic> findByType(StatisticType type, Date init, Date end, Pageable pageable);

    Statistic save(Statistic statistic);

    void remove(Date date);

    void remove(StatisticType type);

}
