package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Type;

import java.util.List;

public interface TypeService {

    List<Type> findAll();

}
