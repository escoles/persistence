package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Speciality;

import java.util.List;
import java.util.Optional;

public interface SpecialitiesService {

    List<Speciality> findAll();

    Optional<Speciality> findById(String id);

}
