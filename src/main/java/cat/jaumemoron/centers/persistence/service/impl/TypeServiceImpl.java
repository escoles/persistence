package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.domain.Type;
import cat.jaumemoron.centers.persistence.repository.TypesRepository;
import cat.jaumemoron.centers.persistence.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypesRepository repository;

    @Override
    @Cacheable(CacheConstants.TYPES_CACHE_KEY)
    public List<Type> findAll() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }
}
