package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.constants.StatisticType;
import cat.jaumemoron.centers.persistence.domain.Statistic;
import cat.jaumemoron.centers.persistence.repository.StatisticsRepository;
import cat.jaumemoron.centers.persistence.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    private StatisticsRepository repository;


    @Override
    public Optional<Statistic> findById(Date date) {
        if (date == null)
            throw new IllegalArgumentException("Statistic date cannot be null");
        return repository.findById(date);
    }

    @Override
    @Cacheable(CacheConstants.STATISTICS_TYPE_CACHE_KEY)
    public Page<Statistic> findByType(StatisticType type, Pageable pageable) {
        if (type == null)
            throw new IllegalArgumentException("Statistic type cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable object cannot be null");
        return repository.findByType(type, pageable);
    }

    @Override
    @Cacheable(CacheConstants.STATISTICS_TYPE_DATE_CACHE_KEY)
    public Page<Statistic> findByType(StatisticType type, Date init, Date end, Pageable pageable) {
        if (type == null)
            throw new IllegalArgumentException("Statistic type cannot be null");
        if (init == null)
            throw new IllegalArgumentException("Init date cannot be null");
        if (end == null)
            throw new IllegalArgumentException("End date cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable object cannot be null");
        return repository.findByTypeAndDateBetween(type, init, end, pageable);
    }

    @Override
    @Transactional
    @CacheEvict(value = {CacheConstants.STATISTICS_TYPE_CACHE_KEY,
            CacheConstants.STATISTICS_TYPE_DATE_CACHE_KEY}, allEntries = true)
    public Statistic save(Statistic statistic) {
        if (statistic == null)
            throw new IllegalArgumentException("Statistic object cannot be null");
        return repository.save(statistic);
    }

    @Override
    @Transactional
    @CacheEvict(value = {CacheConstants.STATISTICS_TYPE_CACHE_KEY,
            CacheConstants.STATISTICS_TYPE_DATE_CACHE_KEY}, allEntries = true)
    public void remove(Date date) {
        if (date == null)
            throw new IllegalArgumentException("Statistic date cannot be null");
        repository.deleteById(date);
    }

    @Override
    @Transactional
    @CacheEvict(value = {CacheConstants.STATISTICS_TYPE_CACHE_KEY,
            CacheConstants.STATISTICS_TYPE_DATE_CACHE_KEY}, allEntries = true)
    public void remove(StatisticType type) {
        if (type == null)
            throw new IllegalArgumentException("Statistic type cannot be null");
        repository.deleteByType(type);
    }
}
