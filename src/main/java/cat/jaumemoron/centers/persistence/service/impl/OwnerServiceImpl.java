package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.domain.Owner;
import cat.jaumemoron.centers.persistence.repository.OwnersRepository;
import cat.jaumemoron.centers.persistence.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OwnerServiceImpl implements OwnerService {

    @Autowired
    private OwnersRepository repository;

    @Override
    @Cacheable(CacheConstants.OWNERS_CACHE_KEY)
    public List<Owner> findAll() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }
}
