package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Action;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Optional;

public interface ActionService {

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    Page<Action> findAll(Pageable pageable);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    Page<Action> findByTerm(String term, Pageable pageable);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    long countUnRead();

    // Usuaris no logats poden crear accions
    Action create(Action action);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    void markAsRead(String id);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    void delete(String id);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    Optional<Action> findById(String id);

}
