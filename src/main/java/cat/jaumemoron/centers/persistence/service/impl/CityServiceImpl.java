package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.domain.City;
import cat.jaumemoron.centers.persistence.repository.CitiesRepository;
import cat.jaumemoron.centers.persistence.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CitiesRepository repository;

    @Override
    @Cacheable(CacheConstants.CITIES_CACHE_KEY)
    public List<City> findAll() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }
}
