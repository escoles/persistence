package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.domain.Center;
import cat.jaumemoron.centers.persistence.exception.DataRequiredException;
import cat.jaumemoron.centers.persistence.repository.CentersRepository;
import cat.jaumemoron.centers.persistence.service.CenterService;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cat.jaumemoron.centers.persistence.utils.PersistenceUtils.toPage;
import static org.elasticsearch.index.query.QueryBuilders.*;

@Service
public class CenterServiceImpl implements CenterService {

    @Autowired
    private CentersRepository repository;

    @Autowired
    private ElasticsearchOperations operations;

    @Override
    public Center save(Center centre) {
        validateMandatoryData(centre);
        return repository.save(centre);
    }

    @Override
    public void delete(Center centre) {
        repository.delete(centre);
    }

    @Override
    public void deleteById(String id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Center> findById(String id) {
        if (ObjectUtils.isEmpty(id))
            throw new IllegalArgumentException("Entity id cannot be null");
        return repository.findById(id);
    }

    @Override
    public Page<Center> findAll(Pageable pageable) {
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findAll(pageable);
    }

    @Override
    public Page<Center> findByTerm(String term, Pageable pageable) {
        String wildcardText = "*" + term + "*";
        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .should(matchPhrasePrefixQuery("name", term))
                        .should(matchQuery("name", term))
                        .should(matchQuery("postalCode", term))
                        .should(matchPhrasePrefixQuery("city.name", term))
                        .should(matchPhrasePrefixQuery("region.name", term))
                        .should(matchPhrasePrefixQuery("territorial.name", term))
                        .should(matchQuery("nature.name", term))
                        .should(nestedQuery("typeList", boolQuery().should(wildcardQuery("typeList.name", wildcardText)), ScoreMode.None))
                        .should(nestedQuery("typeList", boolQuery().should(matchPhrasePrefixQuery("typeList.name", term)), ScoreMode.None)))

                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, Center.class), pageable);
    }

    @Override
    public Page<Center> findByName(String name, Pageable pageable) {
        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .should(matchQuery("name", name)))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, Center.class), pageable);
    }

    @Override
    public Page<Center> findByType(String type, Pageable pageable) {
        if (type == null)
            throw new IllegalArgumentException("Type cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");

        var searchQuery = new NativeSearchQueryBuilder().withQuery
                        (nestedQuery("typeList", boolQuery().must(matchQuery("typeList.id", type)), ScoreMode.None))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, Center.class), pageable);
    }

    @Override
    public Page<Center> findByCity(String cityId, Pageable pageable) {
        if (ObjectUtils.isEmpty(cityId))
            throw new IllegalArgumentException("City id cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findByCityId(cityId, pageable);
    }

    @Override
    public Page<Center> findByTypeAndCity(String type, String cityId, Pageable pageable) {
        if (type == null)
            throw new IllegalArgumentException("Type cannot be null");
        if (ObjectUtils.isEmpty(cityId))
            throw new IllegalArgumentException("City id cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(matchQuery("city.id", cityId))
                        .must(nestedQuery("typeList", boolQuery().must(matchQuery("typeList.id", type)), ScoreMode.None)))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, Center.class), pageable);
    }


    @Override
    public Page<Center> findByTypeAndRegion(String type, String regionId, Pageable pageable) {
        if (type == null)
            throw new IllegalArgumentException("Type cannot be null");
        if (ObjectUtils.isEmpty(regionId))
            throw new IllegalArgumentException("Region id cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .must(matchQuery("region.id", regionId))
                        .must(nestedQuery("typeList", boolQuery().must(matchQuery("typeList.id", type)), ScoreMode.None)))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, Center.class), pageable);
    }

    @Override
    public Page<Center> findByRegion(String regionId, Pageable pageable) {
        if (ObjectUtils.isEmpty(regionId))
            throw new IllegalArgumentException("Region id cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findByRegionId(regionId, pageable);
    }

    @Override
    public Page<Center> findByNameContaining(String name, Pageable pageable) {
        if (ObjectUtils.isEmpty(name))
            throw new IllegalArgumentException("Name cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findByNameContaining(name, pageable);
    }

    public Page<Center> findByLocation(double latitude, double longitude, int distance, Pageable pageable) {
        if (latitude == 0)
            throw new IllegalArgumentException("Lattitude cannot be 0");
        if (longitude == 0)
            throw new IllegalArgumentException("Longitude cannot be 0");
        if (distance == 0)
            throw new IllegalArgumentException("Distance cannot be 0");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        String sDistance = distance + DistanceUnit.KILOMETERS.toString();

        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.geoDistanceQuery("location").point(latitude, longitude).distance(sDistance))
                .withSort(SortBuilders.geoDistanceSort("location", latitude, longitude))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, Center.class), pageable);
    }

    private void validateMandatoryData(Center centre) {
        List<String> errorList = new ArrayList<>();
        if (centre == null)
            throw new IllegalArgumentException("Center cannot be null");
        if (ObjectUtils.isEmpty(centre.getName()))
            errorList.add("Name cannot be null");
        if (ObjectUtils.isEmpty(centre.getAddress()))
            errorList.add("Address cannot be null");
        if (ObjectUtils.isEmpty(centre.getPostalCode()))
            errorList.add("Postal code cannot be null");
        if (centre.getCity() == null)
            errorList.add("City cannot be null");
        if (centre.getRegion() == null)
            errorList.add("Region cannot be null");
        if (centre.getLocation() == null)
            errorList.add("Location cannot be null");
        if (centre.getLastModificationDate() == null)
            errorList.add("Last modification date cannot be null");
        if (!errorList.isEmpty())
            throw new DataRequiredException("Data required to save/udpate Centre entity", errorList);
    }

    public Page<Center> findByTerritorial(String territorialId, Pageable pageable) {
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("TerritorialId cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findByTerritorialId(territorialId, pageable);
    }

    public Page<Center> findByTerritorialAndTerm(String territorialId, String term, Pageable pageable) {
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("TerritorialId cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        String wildcardText = "*" + term + "*";
        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .should(matchPhrasePrefixQuery("name", term))
                        .should(matchQuery("postalCode", term))
                        .should(matchPhrasePrefixQuery("city.name", term))
                        .should(matchPhrasePrefixQuery("region.name", term))
                        .should(matchQuery("nature.name", term))
                        .should(nestedQuery("typeList", boolQuery().should(wildcardQuery("typeList.name", wildcardText)), ScoreMode.None)))
                .withFilter(QueryBuilders.boolQuery()
                        .must(matchQuery("territorial.id", territorialId)))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, Center.class), pageable);
    }

    public Page<Center> findByTerritorialAndName(String territorialId, String name, Pageable pageable) {
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("TerritorialId cannot be null");
        if (ObjectUtils.isEmpty(name))
            throw new IllegalArgumentException("Name cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        var searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.boolQuery()
                        .should(matchQuery("name", name)))
                .withFilter(QueryBuilders.boolQuery()
                        .must(matchQuery("territorial.id", territorialId)))
                .withPageable(pageable).build();
        return toPage(operations.search(searchQuery, Center.class), pageable);
    }

    public Page<Center> findByNature(String natureIdId, Pageable pageable) {
        if (ObjectUtils.isEmpty(natureIdId))
            throw new IllegalArgumentException("Nature Id cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findByNatureId(natureIdId, pageable);
    }

    public Page<Center> findByOwner(String ownerId, Pageable pageable) {
        if (ObjectUtils.isEmpty(ownerId))
            throw new IllegalArgumentException("Owner Id cannot be null");
        if (pageable == null)
            throw new IllegalArgumentException("Pageable cannot be null");
        return repository.findByOwnerId(ownerId, pageable);
    }

    @Override
    @Cacheable(CacheConstants.TOTAL_CENTERS_CACHE_KEY)
    public long count() {
        return repository.count();
    }

    @Override
    @Cacheable(CacheConstants.CENTER_TERRITORIAL_CACHE_KEY)
    public long countByTerritorial(String territorialId) {
        if (ObjectUtils.isEmpty(territorialId))
            throw new IllegalArgumentException("TerritorialId cannot be null");
        return repository.countByTerritorialId(territorialId);
    }
}
