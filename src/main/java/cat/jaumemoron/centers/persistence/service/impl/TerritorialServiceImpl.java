package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.domain.Territorial;
import cat.jaumemoron.centers.persistence.repository.TerritorialsRepository;
import cat.jaumemoron.centers.persistence.service.TerritorialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;

@Service
public class TerritorialServiceImpl implements TerritorialService {

    @Autowired
    private TerritorialsRepository repository;

    @Override
    @Cacheable(CacheConstants.TERRITORIALS_CACHE_KEY)
    public List<Territorial> findAll() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Cacheable(CacheConstants.TERRITORIAL_ID_CACHE_KEY)
    public Optional<Territorial> findById(String id) {
        if (ObjectUtils.isEmpty(id))
            throw new IllegalArgumentException("Territorial id cannot be null");
        return repository.findById(id);
    }

    @Cacheable(CacheConstants.TERRITORIALS_APP_ID_CACHE_KEY)
    public Optional<Territorial> findByAppointmentId(String appointmentId) {
        if (ObjectUtils.isEmpty(appointmentId))
            throw new IllegalArgumentException("appointmentId id cannot be null");
        return repository.findByAppointmentId(appointmentId);
    }
}
