package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Territorial;

import java.util.List;
import java.util.Optional;

public interface TerritorialService {

    List<Territorial> findAll();

    Optional<Territorial> findById(String id);

    Optional<Territorial> findByAppointmentId(String appointmentId);

}
