package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.domain.AppointmentData;
import cat.jaumemoron.centers.persistence.domain.AppointmentDataDayType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface AppointmentDataService {

    Page<AppointmentData> findBySeasonAndTerritorial(String season, String territorialId, Pageable pageable);

    Page<AppointmentData> findBySeasonAndTerritorialAndSpeciality(String season, String territorialId, String specialityId, Pageable pageable);

    Page<AppointmentDataDayType> findBySeasonAndTerritorialIdAndDayType(String season, String territorialId, DayType dayType, Pageable pageable);

    Page<AppointmentDataDayType> findBySeasonAndTerritorialIdAndDayTypeAll(String season, String territorialId, Pageable pageable);

    Page<AppointmentDataDayType> findBySeasonAndTerritorialAndSpecialityAndDayType
            (String season, String territorialId, String specialityId, DayType dayType, Pageable pageable);

    Page<AppointmentDataDayType> findBySeasonAndTerritorialAndSpecialityAllDayType
            (String season, String territorialId, String specialityId, Pageable pageable);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    void update();


}
