package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.domain.Region;
import cat.jaumemoron.centers.persistence.repository.RegionsRepository;
import cat.jaumemoron.centers.persistence.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionServiceImpl implements RegionService {

    @Autowired
    private RegionsRepository repository;

    @Override
    @Cacheable(CacheConstants.REGIONS_CACHE_KEY)
    public List<Region> findAll() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }
}
