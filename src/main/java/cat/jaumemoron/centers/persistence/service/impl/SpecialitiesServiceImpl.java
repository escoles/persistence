package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.domain.Speciality;
import cat.jaumemoron.centers.persistence.repository.SpecialitiesRepository;
import cat.jaumemoron.centers.persistence.service.SpecialitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;

@Service
public class SpecialitiesServiceImpl implements SpecialitiesService {

    @Autowired
    private SpecialitiesRepository repository;

    @Override
    @Cacheable(CacheConstants.SPECIALITIES_CACHE_KEY)
    public List<Speciality> findAll() {
        return repository.findAllByOrderByNameAsc();
    }

    @Override
    @Cacheable(CacheConstants.SPECIALITIES_CACHE_NAME_KEY)
    public Optional<Speciality> findById(String id) {
        if (ObjectUtils.isEmpty(id))
            throw new IllegalArgumentException("Speciality id cannot be null");
        return repository.findById(id);
    }
}

