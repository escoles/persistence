package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Region;

import java.util.List;

public interface RegionService {

    List<Region> findAll();

}
