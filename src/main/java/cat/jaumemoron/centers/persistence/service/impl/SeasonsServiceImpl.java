package cat.jaumemoron.centers.persistence.service.impl;

import cat.jaumemoron.centers.persistence.constants.CacheConstants;
import cat.jaumemoron.centers.persistence.domain.Season;
import cat.jaumemoron.centers.persistence.repository.SeasonsRepository;
import cat.jaumemoron.centers.persistence.service.SeasonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;

@Service
public class SeasonsServiceImpl implements SeasonsService {

    @Autowired
    private SeasonsRepository repository;

    @Override
    @Cacheable(CacheConstants.SEASONS_CACHE_KEY)
    public List<Season> findAll() {
        return repository.findAllByOrderByIdDesc();
    }

    @Override
    @CacheEvict(value = {CacheConstants.SEASONS_CACHE_KEY, CacheConstants.SEASONS_CACHE_ID_KEY}, allEntries = true)
    public Season create(Season season) {
        if (season.getId() == null)
            throw new IllegalArgumentException("Season id cannot be null");
        return repository.save(season);
    }

    @Override
    @Cacheable(CacheConstants.SEASONS_CACHE_ID_KEY)
    public Optional<Season> findById(String id) {
        if (ObjectUtils.isEmpty(id))
            throw new IllegalArgumentException("Season id cannot be null");
        return repository.findById(id);
    }
}

