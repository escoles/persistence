package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.domain.Center;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Optional;

public interface CenterService {

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    Center save(Center centre);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    void delete(Center centre);

    @PreAuthorize("hasRole(T(cat.jaumemoron.centers.persistence.constants.PersistenceConstants).ROLE_ADMIN)")
    void deleteById(String id);

    Optional<Center> findById(String id);

    Page<Center> findAll(Pageable pageable);

    Page<Center> findByTerm(String term, Pageable pageable);

    Page<Center> findByName(String name, Pageable pageable);

    Page<Center> findByType(String type, Pageable pageable);

    Page<Center> findByTypeAndCity(String type, String cityId, Pageable pageable);

    Page<Center> findByTypeAndRegion(String type, String regionId, Pageable pageable);

    Page<Center> findByCity(String cityId, Pageable pageable);

    Page<Center> findByRegion(String regionId, Pageable pageable);

    Page<Center> findByNameContaining(String name, Pageable pageable);

    Page<Center> findByLocation(double latitude, double longitude, int distance, Pageable pageable);

    Page<Center> findByTerritorial(String territorialId, Pageable pageable);

    Page<Center> findByTerritorialAndTerm(String territorialId, String term, Pageable pageable);

    Page<Center> findByTerritorialAndName(String territorialId, String name, Pageable pageable);

    Page<Center> findByNature(String natureIdId, Pageable pageable);

    Page<Center> findByOwner(String ownerId, Pageable pageable);

    long count();

    long countByTerritorial(String territorialId);
}
