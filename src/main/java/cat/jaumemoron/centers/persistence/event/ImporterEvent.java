package cat.jaumemoron.centers.persistence.event;

import cat.jaumemoron.centers.persistence.importer.GenericImporterResult;

public class ImporterEvent extends PersistenceEvent<GenericImporterResult> {

    public ImporterEvent(GenericImporterResult result) {
        super(result);
    }
}
