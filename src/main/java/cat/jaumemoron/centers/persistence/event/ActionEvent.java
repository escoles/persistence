package cat.jaumemoron.centers.persistence.event;

import cat.jaumemoron.centers.persistence.domain.Action;

import java.util.Date;

public class ActionEvent extends PersistenceEvent<Action> {

    public ActionEvent(String text) {
        super(Action.of(text, new Date()));
    }

    public ActionEvent(Action action) {
        super(action);
    }
}
