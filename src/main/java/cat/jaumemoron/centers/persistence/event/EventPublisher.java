package cat.jaumemoron.centers.persistence.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

public abstract class EventPublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventPublisher.class);

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    public void publish(PersistenceEvent event) {
        LOGGER.debug("Publishing event {}", event);
        applicationEventPublisher.publishEvent(event);
    }

}