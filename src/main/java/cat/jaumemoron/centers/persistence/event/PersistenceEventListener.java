package cat.jaumemoron.centers.persistence.event;

import cat.jaumemoron.centers.persistence.constants.StatisticType;
import cat.jaumemoron.centers.persistence.domain.Action;
import cat.jaumemoron.centers.persistence.domain.Statistic;
import cat.jaumemoron.centers.persistence.importer.GenericImporterResult;
import cat.jaumemoron.centers.persistence.service.ActionService;
import cat.jaumemoron.centers.persistence.service.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Component
public class PersistenceEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceEventListener.class);

    @Autowired
    private ActionService actionService;

    @Autowired
    private StatisticsService statisticsService;

    @EventListener
    @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    public void handleNewActionEvent(ActionEvent event) {
        if (mustBeProcessed(event))
            actionService.create(event.getObject());
    }

    @EventListener
    @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    public void handle(ImporterEvent event) {
        if (mustBeProcessed(event)) {
            GenericImporterResult result = event.getObject();
            Action action;
            if (result.hasErrors()) {
                action = Action.of("File [" + result.getFile() + "] processed with errors. " +
                        "Processed: " + result.getRecordsNumber() + " records taking " + result.getProcessTime() + " millis");
                for (String error : result.getErrors()) {
                    action.addComment(error);
                }
            } else
                action = Action.of("File [" + result.getFile() + "] processed successfully. " +
                        "Processed: " + result.getRecordsNumber() + " records taking " + result.getProcessTime() + " millis");
            actionService.create(action);
        }
    }

    private boolean mustBeProcessed(PersistenceEvent event) {
        LOGGER.debug("Processing event {}", event);
        if (event == null || event.getObject() == null) {
            LOGGER.warn("Trying to process a null event", event);
            return false;
        } else
            return true;
    }

    @EventListener
    @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    public void handle(TelegramRequestEvent event) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(sdf.format(event.getDate()));
            Optional<Statistic> optional = statisticsService.findById(date);
            Statistic statistic;
            if (!optional.isPresent()) {
                statistic = new Statistic();
                statistic.setDate(date);
                statistic.setType(StatisticType.TELEGRAM_REQUEST);
            } else
                statistic = optional.get();
            if (event.getObject())
                statistic.setNumberOfOks(statistic.getNumberOfOks() + 1L);
            else
                statistic.setNumberOfErrors(statistic.getNumberOfErrors() + 1L);
            statisticsService.save(statistic);
        } catch (ParseException e) {
            LOGGER.error("Error parsing date when try to update a statistic", e);
        }
    }

}
