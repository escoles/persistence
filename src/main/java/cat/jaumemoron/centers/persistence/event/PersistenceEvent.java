package cat.jaumemoron.centers.persistence.event;

public class PersistenceEvent<T> {

    private T object;

    protected PersistenceEvent(T object) {
        this.object = object;
    }

    public T getObject() {
        return object;
    }

    @Override
    public String toString() {
        return "PersistenceEvent{" +
                "object=" + object +
                '}';
    }
}
