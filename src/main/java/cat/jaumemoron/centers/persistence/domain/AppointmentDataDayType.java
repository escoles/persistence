package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.constants.IndexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = IndexConstants.APPOINTMENT_DATA_DAYTYPE_NAME)
@ApiModel(value = "AppointmentDataDayType", description = "Object with the appointment data grouped by speciality and daytype")
public class AppointmentDataDayType extends GenericAppointmentData {

    @Field(type = FieldType.Text, fielddata = true)
    @ApiModelProperty(value = "The work day type", required = true)
    private DayType dayType;

    public DayType getDayType() {
        return dayType;
    }

    public void setDayType(DayType dayType) {
        this.dayType = dayType;
    }

    public static final class Builder {
        private String id;
        private String season;
        private AppointmentTerritorial territorial;
        private AppointmentSpeciality speciality;
        private AppointmentNumberData data;
        private DayType dayType;

        private Builder() {
        }

        public static AppointmentDataDayType.Builder anAppointmentData() {
            return new AppointmentDataDayType.Builder();
        }

        public AppointmentDataDayType.Builder withId(String id) {
            this.id = id;
            return this;
        }

        public AppointmentDataDayType.Builder withSeason(String season) {
            this.season = season;
            return this;
        }

        public AppointmentDataDayType.Builder withTerritorial(AppointmentTerritorial territorial) {
            this.territorial = territorial;
            return this;
        }

        public AppointmentDataDayType.Builder withSpeciality(AppointmentSpeciality speciality) {
            this.speciality = speciality;
            return this;
        }

        public AppointmentDataDayType.Builder withData(AppointmentNumberData data) {
            this.data = data;
            return this;
        }

        public AppointmentDataDayType.Builder withDayType(DayType dayType) {
            this.dayType = dayType;
            return this;
        }

        public AppointmentDataDayType build() {
            AppointmentDataDayType appointmentData = new AppointmentDataDayType();
            appointmentData.setId(id);
            appointmentData.setSeason(season);
            appointmentData.setTerritorial(territorial);
            appointmentData.setSpeciality(speciality);
            appointmentData.setData(data);
            appointmentData.setDayType(dayType);
            return appointmentData;
        }
    }
}
