package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.IndexConstants;
import io.swagger.annotations.ApiModel;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = IndexConstants.APPOINTMENT_DATA_NAME)
@ApiModel(value = "AppointmentData", description = "Object with the appointment data grouped by speciality")
public class AppointmentData extends GenericAppointmentData {

    public static final class Builder {
        private String id;
        private String season;
        private AppointmentTerritorial territorial;
        private AppointmentSpeciality speciality;
        private AppointmentNumberData data;

        private Builder() {
        }

        public static Builder anAppointmentData() {
            return new Builder();
        }

        public Builder withSeason(String season) {
            this.season = season;
            return this;
        }

        public Builder withTerritorial(AppointmentTerritorial territorial) {
            this.territorial = territorial;
            return this;
        }

        public Builder withSpeciality(AppointmentSpeciality speciality) {
            this.speciality = speciality;
            return this;
        }

        public Builder withData(AppointmentNumberData data) {
            this.data = data;
            return this;
        }

        public AppointmentData build() {
            AppointmentData appointmentData = new AppointmentData();
            appointmentData.setId(id);
            appointmentData.setSeason(season);
            appointmentData.setTerritorial(territorial);
            appointmentData.setSpeciality(speciality);
            appointmentData.setData(data);
            return appointmentData;
        }
    }
}
