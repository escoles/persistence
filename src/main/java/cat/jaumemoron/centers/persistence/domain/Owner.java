package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.IndexConstants;
import io.swagger.annotations.ApiModel;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = IndexConstants.DICTIONARY_NAME)
@ApiModel(value = "Owner", description = "Entity with the owner data")
public class Owner extends Dictionary {

    private Owner() {
        super();
    }

    private Owner(String id, String name) {
        super(id, name);
    }

    public static Owner of(String id, String name) {
        return new Owner(id, name);
    }

}

