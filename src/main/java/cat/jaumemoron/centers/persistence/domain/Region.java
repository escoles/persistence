package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.IndexConstants;
import io.swagger.annotations.ApiModel;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = IndexConstants.DICTIONARY_NAME)
@ApiModel(value = "Region", description = "Entity with the region data")
public class Region extends Dictionary {

    private Region() {
        super();
    }

    private Region(String id, String name) {
        super(id, name);
    }

    public static Region of(String id, String name) {
        return new Region(id, name);
    }
}

