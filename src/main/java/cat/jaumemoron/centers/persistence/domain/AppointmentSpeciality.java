package cat.jaumemoron.centers.persistence.domain;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Appointment Speciality", description = "Entity with the speciality data")
public class AppointmentSpeciality extends Dictionary {

    private AppointmentSpeciality() {
        super();
    }

    private AppointmentSpeciality(String id, String name) {
        super(id, name);
    }

    public static AppointmentSpeciality of(String id, String name) {
        return new AppointmentSpeciality(id, name);
    }


}

