package cat.jaumemoron.centers.persistence.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SEASONS")
@ApiModel(value = "Season", description = "Entity with the season data")
public class Season {

    @Id
    @Column(name = "ID", length = 10)
    @ApiModelProperty(value = "The unique season id")
    private String id;

    public Season() {
    }

    private Season(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static Season of(String id) {
        return new Season(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Season)) return false;

        Season season = (Season) o;

        return getId().equals(season.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
