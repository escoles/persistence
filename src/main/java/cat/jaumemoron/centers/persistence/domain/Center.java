package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.IndexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(indexName = IndexConstants.CENTERS_NAME)
@ApiModel(value = "Center", description = "Object with the center data")
public class Center {

    @Id
    @Field(type = FieldType.Text)
    @ApiModelProperty(value = "The center unique id", required = true)
    private String id;

    @Field(type = FieldType.Nested)
    @ApiModelProperty(value = "The center educational type list", required = true)
    private List<Type> typeList = new ArrayList<Type>();

    @Field(type = FieldType.Text, fielddata = true)
    @ApiModelProperty(value = "The center name", required = true)
    private String name;

    @Field(type = FieldType.Text, index = false)
    @ApiModelProperty(value = "The center address", required = true)
    private String address;

    @Field(type = FieldType.Text)
    @ApiModelProperty(value = "The center postal code", required = true)
    private String postalCode;

    @Field(type = FieldType.Object)
    @ApiModelProperty(value = "The city of the center", required = true)
    private City city;

    @Field(type = FieldType.Object)
    @ApiModelProperty(value = "The region of the center", required = true)
    private Region region;

    @Field(type = FieldType.Text, index = false)
    @ApiModelProperty(value = "A list of phones")
    private List<String> phoneList = new ArrayList<>();

    @Field(type = FieldType.Text, index = false)
    @ApiModelProperty(value = "A list of emails")
    private List<String> emailList = new ArrayList<>();

    @Field(type = FieldType.Text, index = false)
    @ApiModelProperty(value = "The center fax")
    private String fax;

    @ApiModelProperty(value = "The center location (latitude and longitude)")
    private GeoPoint location;

    @Field(type = FieldType.Date, index = false)
    private Date lastModificationDate;

    @Field(type = FieldType.Object)
    @ApiModelProperty(value = "The territorial of the center", required = true)
    private Territorial territorial;

    @Field(type = FieldType.Object)
    @ApiModelProperty(value = "The nature of the center", required = true)
    private Nature nature;

    @Field(type = FieldType.Object)
    @ApiModelProperty(value = "The owner of the center", required = true)
    private Owner owner;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Type> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<Type> typeList) {
        this.typeList = typeList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<String> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<String> phoneList) {
        this.phoneList = phoneList;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public void addTelefon(String telefon) {
        this.phoneList.add(telefon);
    }

    public void addEmail(String email) {
        this.emailList.add(email);
    }

    public Territorial getTerritorial() {
        return territorial;
    }

    public void setTerritorial(Territorial territorial) {
        this.territorial = territorial;
    }

    public Nature getNature() {
        return nature;
    }

    public void setNature(Nature nature) {
        this.nature = nature;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public void addType(Type type) {
        this.typeList.add(type);
    }

    public static final class Builder {
        private String id;
        private List<Type> typeList = new ArrayList<>();
        private String name;
        private String address;
        private String postalCode;
        private City city;
        private Region region;
        private List<String> phones = new ArrayList<>();
        private List<String> emails = new ArrayList<>();
        private String fax;
        private GeoPoint location;
        private Date lastModificationDate;
        private Territorial territorial;
        private Nature nature;
        private Owner owner;

        private Builder() {
        }

        public static Builder aCenter() {
            return new Builder();
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withType(Type type) {
            typeList.add(type);
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder withPostalCode(String postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public Builder withCity(City city) {
            this.city = city;
            return this;
        }

        public Builder withRegion(Region region) {
            this.region = region;
            return this;
        }

        public Builder withPhone(String phone) {
            if (phone != null && !phone.isEmpty())
                this.phones.add(phone);
            return this;
        }

        public Builder withEmail(String email) {
            if (email != null && !email.isEmpty())
                this.emails.add(email);
            return this;
        }

        public Builder withFax(String fax) {
            this.fax = fax;
            return this;
        }

        public Builder withLocation(GeoPoint location) {
            this.location = location;
            return this;
        }

        public Builder withLocation(double latitud, double longitud) {
            this.location = new GeoPoint(latitud, longitud);
            return this;
        }

        public Builder withLastModificationDate(Date lastModificationDate) {
            this.lastModificationDate = lastModificationDate;
            return this;
        }

        public Builder withTerritorial(Territorial territorial) {
            this.territorial = territorial;
            return this;
        }

        public Builder withNature(Nature nature) {
            this.nature = nature;
            return this;
        }

        public Builder withOwner(Owner owner) {
            this.owner = owner;
            return this;
        }

        public Center build() {
            Center center = new Center();
            center.setId(id);
            center.setTypeList(typeList);
            center.setName(name);
            center.setAddress(address);
            center.setPostalCode(postalCode);
            center.setCity(city);
            center.setRegion(region);
            center.setPhoneList(phones);
            center.setEmailList(emails);
            center.setFax(fax);
            center.setLocation(location);
            center.setLastModificationDate(lastModificationDate);
            center.setTerritorial(territorial);
            center.setNature(nature);
            center.setOwner(owner);
            return center;
        }
    }

    @Override
    public String toString() {
        return "Center{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
