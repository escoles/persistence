package cat.jaumemoron.centers.persistence.domain;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

public abstract class Dictionary {

    @Id
    @Field(type = FieldType.Text, fielddata = true)
    @ApiModelProperty(value = "The unique id", required = true)
    private String id;

    @Field(type = FieldType.Text, fielddata = true)
    @ApiModelProperty(value = "The description", required = true)
    private String name;

    Dictionary() {
    }

    Dictionary(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dictionary)) return false;

        Dictionary that = (Dictionary) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public String toString() {
        return id + " - " + name;
    }
}
