package cat.jaumemoron.centers.persistence.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TERRITORIALS")
@ApiModel(value = "Territorial", description = "Entity with the territorial data")
public class Territorial {

    @Id
    @Column(name = "ID", length = 4)
    @ApiModelProperty(value = "Territorial id", required = true)
    private String id;

    @Column(name = "NAME", length = 40)
    @ApiModelProperty(value = "Territorial name", required = true)
    private String name;

    @Column(name = "APPOINTMENT_ID", length = 4)
    @ApiModelProperty(value = "Appointment id", required = true)
    private String appointmentId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    private Territorial() {
        super();
    }

    private Territorial(String id, String name) {
        this.id = id;
        this.name = name;
    }

    private Territorial(String id, String name, String appointmentId) {
        this(id, name);
        this.appointmentId = appointmentId;
    }

    public static Territorial of(String id, String name) {
        return new Territorial(id, name, id);
    }

    public static Territorial of(String id, String name, String appointmentId) {
        return new Territorial(id, name, appointmentId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Territorial)) return false;

        Territorial that = (Territorial) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}

