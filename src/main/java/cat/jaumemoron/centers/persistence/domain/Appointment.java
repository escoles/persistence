package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.DayType;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "APPOINTMENTS")
public class Appointment {

    @Id
    @Column(name = "ID", length = 20)
    @ApiModelProperty(value = "The Appointment unique id", required = true)
    private String id;

    @ManyToOne
    @JoinColumn(name = "TERRITORIAL_ID")
    @ApiModelProperty(value = "The Appointment territorial", required = true)
    private Territorial territorial;

    @ManyToOne
    @JoinColumn(name = "SPECIALITY_ID")
    @ApiModelProperty(value = "The appointment speciality", required = true)
    private Speciality speciality;

    @ManyToOne
    @JoinColumn(name = "SEASON_ID")
    @ApiModelProperty(value = "The Appointment season", required = true)
    private Season season;

    @Column(name = "CENTER_NAME", length = 100)
    @ApiModelProperty(value = "The center name where appointment is assigned")
    private String centerName;

    @Column(name = "NUMBER")
    @ApiModelProperty(value = "The appointment number", required = true)
    private long number;

    @Column(name = "APPOINTMENT_DATE")
    @ApiModelProperty(value = "The appointment date", required = true)
    private Date date;

    @Column(name = "DAYTYPE")
    @ApiModelProperty(value = "The type of working day")
    private DayType dayType;

    @Column(name = "INIT_DATE")
    @ApiModelProperty(value = "The appointment init date")
    private Date init;

    @Column(name = "END_DATE")
    @ApiModelProperty(value = "The appointment end date")
    private Date end;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Territorial getTerritorial() {
        return territorial;
    }

    public void setTerritorial(Territorial territorial) {
        this.territorial = territorial;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public DayType getDayType() {
        return dayType;
    }

    public void setDayType(DayType dayType) {
        this.dayType = dayType;
    }

    public Date getInit() {
        return init;
    }

    public void setInit(Date init) {
        this.init = init;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
