package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.IndexConstants;
import io.swagger.annotations.ApiModel;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = IndexConstants.DICTIONARY_NAME)
@ApiModel(value = "Type", description = "The center educational level")
public class Type extends Dictionary {

    private Type() {
        super();
    }

    private Type(String id, String name) {
        super(id, name);
    }

    public static Type of(String id, String name) {
        return new Type(id, name);
    }

}

