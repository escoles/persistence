package cat.jaumemoron.centers.persistence.domain;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

public abstract class GenericAppointmentData {

    @Id
    @Field(type = FieldType.Text)
    @ApiModelProperty(value = "The Appointment data unique id", required = true)
    private String id;

    @Field(type = FieldType.Text, fielddata = true)
    @ApiModelProperty(value = "The season appointment", required = true)
    private String season;

    @Field(type = FieldType.Object)
    @ApiModelProperty(value = "The territorial that appointment belongs", required = true)
    private AppointmentTerritorial territorial;

    @Field(type = FieldType.Object)
    @ApiModelProperty(value = "The speciality of the appointment", required = true)
    private AppointmentSpeciality speciality;

    @Field(type = FieldType.Object)
    @ApiModelProperty(value = "The speciality of the appointment", required = true)
    private AppointmentNumberData data;

    @Field(type = FieldType.Long)
    @ApiModelProperty(value = "The total number of appointments", required = true)
    private long count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public AppointmentTerritorial getTerritorial() {
        return territorial;
    }

    public void setTerritorial(AppointmentTerritorial territorial) {
        this.territorial = territorial;
    }

    public AppointmentSpeciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(AppointmentSpeciality speciality) {
        this.speciality = speciality;
    }

    public AppointmentNumberData getData() {
        return data;
    }

    public void setData(AppointmentNumberData data) {
        this.data = data;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GenericAppointmentData)) return false;

        GenericAppointmentData that = (GenericAppointmentData) o;

        if (!getSeason().equals(that.getSeason())) return false;
        if (!getTerritorial().equals(that.getTerritorial())) return false;
        return getSpeciality().equals(that.getSpeciality());
    }

    @Override
    public int hashCode() {
        int result = getSeason().hashCode();
        result = 31 * result + getTerritorial().hashCode();
        result = 31 * result + getSpeciality().hashCode();
        return result;
    }
}
