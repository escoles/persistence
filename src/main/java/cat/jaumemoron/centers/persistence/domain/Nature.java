package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.IndexConstants;
import io.swagger.annotations.ApiModel;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = IndexConstants.DICTIONARY_NAME)
@ApiModel(value = "Nature", description = "Entity with the nature data")
public class Nature extends Dictionary {

    private Nature() {
        super();
    }

    private Nature(String id, String name) {
        super(id, name);
    }

    public static Nature of(String id, String name) {
        return new Nature(id, name);
    }

}

