package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.StatisticType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "STATISTICS")
@ApiModel(value = "Statistic", description = "Entity with the statistic data")
public class Statistic {

    @Id
    @Column(name = "ID", length = 4)
    @ApiModelProperty(value = "Statistic date", required = true)
    private Date date;

    @Column(name = "STATISTIC_TYPE")
    @ApiModelProperty(value = "Statistic type", required = true)
    private StatisticType type;

    @Column(name = "NUMBER_OF_OKS")
    @ApiModelProperty(value = "The oks number", required = true)
    private Long numberOfOks = 0L;

    @Column(name = "NUMBER_OF_ERRORS")
    @ApiModelProperty(value = "The errors number", required = true)
    private Long numberOfErrors = 0L;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public StatisticType getType() {
        return type;
    }

    public void setType(StatisticType type) {
        this.type = type;
    }

    public Long getNumberOfOks() {
        return numberOfOks;
    }

    public void setNumberOfOks(Long numberOfOks) {
        this.numberOfOks = numberOfOks;
    }

    public Long getNumberOfErrors() {
        return numberOfErrors;
    }

    public void setNumberOfErrors(Long numberOfErrors) {
        this.numberOfErrors = numberOfErrors;
    }

}

