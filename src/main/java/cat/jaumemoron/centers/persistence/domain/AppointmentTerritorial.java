package cat.jaumemoron.centers.persistence.domain;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Appointment Territorial", description = "Entity with the territorial data")
public class AppointmentTerritorial extends Dictionary {

    private AppointmentTerritorial() {
        super();
    }

    private AppointmentTerritorial(String id, String name) {
        super(id, name);
    }

    public static AppointmentTerritorial of(String id, String name) {
        return new AppointmentTerritorial(id, name);
    }

}

