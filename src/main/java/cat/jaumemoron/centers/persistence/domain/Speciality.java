package cat.jaumemoron.centers.persistence.domain;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SPECIALITIES")
public class Speciality {

    @Id
    @Column(name = "ID", length = 3)
    @ApiModelProperty(value = "Speciality id")
    private String id;

    @Column(name = "NAME", length = 100)
    @ApiModelProperty(value = "Speciality name")
    private String name;

    private Speciality() {
    }

    private Speciality(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Speciality of(String id, String name) {
        return new Speciality(id, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Speciality)) return false;

        Speciality that = (Speciality) o;

        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }
}
