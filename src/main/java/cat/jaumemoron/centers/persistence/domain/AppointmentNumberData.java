package cat.jaumemoron.centers.persistence.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

@ApiModel(value = "AppointmentNumberData", description = "Object with the appointment number data")
public class AppointmentNumberData {

    @ApiModelProperty(value = "Max appointment number")
    private long maxNumber = 0;
    @Field(type = FieldType.Date)
    @ApiModelProperty(value = "Date of max appointment")
    private Date maxDate;
    @ApiModelProperty(value = "Last appointment number")
    private long lastNumber = 0;
    @Field(type = FieldType.Date)
    @ApiModelProperty(value = "Date of last appointment")
    private Date lastDate;

    public AppointmentNumberData() {
    }

    private AppointmentNumberData(long maxNumber, Date maxDate) {
        this.maxNumber = maxNumber;
        this.maxDate = maxDate;
    }

    public static AppointmentNumberData of(long maxNumber, Date maxDate) {
        return new AppointmentNumberData(maxNumber, maxDate);
    }

    public long getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(long maxNumber) {
        this.maxNumber = maxNumber;
    }

    public Date getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    public long getLastNumber() {
        return lastNumber;
    }

    public void setLastNumber(long lastNumber) {
        this.lastNumber = lastNumber;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }
}
