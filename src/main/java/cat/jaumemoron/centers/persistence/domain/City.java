package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.IndexConstants;
import io.swagger.annotations.ApiModel;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = IndexConstants.DICTIONARY_NAME)
@ApiModel(value = "City", description = "Entity with the city data")
public class City extends Dictionary {

    private City() {
        super();
    }

    private City(String id, String name) {
        super(id, name);
    }

    public static City of(String id, String name) {
        return new City(id, name);
    }

}

