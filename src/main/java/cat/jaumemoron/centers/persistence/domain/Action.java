package cat.jaumemoron.centers.persistence.domain;

import cat.jaumemoron.centers.persistence.constants.IndexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(indexName = IndexConstants.ACTIONS_NAME)
@ApiModel(value = "Action", description = "Representation of a system action")
public class Action {

    @Id
    @ApiModelProperty(value = "The action unique id", required = true)
    private String id;

    @Field(type = FieldType.Text)
    @ApiModelProperty(value = "The description of the action", required = true)
    private String text;

    @Field(type = FieldType.Date, index = false)
    @ApiModelProperty(value = "The date when action was created", required = true)
    private Date date;

    @Field(type = FieldType.Boolean)
    @ApiModelProperty(value = "Flag to know if action was read", required = true)
    private Boolean read = Boolean.FALSE;

    @Field(type = FieldType.Text)
    @ApiModelProperty(value = "A list of comments")
    private List<String> comments = new ArrayList<String>();

    private Action() {
    }

    private Action(String text, Date date) {
        this.text = text;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean isRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public void addComment(String comment) {
        this.comments.add(comment);
    }

    public static Action of(String text) {
        return new Action(text, new Date());
    }

    public static Action of(String text, Date date) {
        return new Action(text, date);
    }
}
