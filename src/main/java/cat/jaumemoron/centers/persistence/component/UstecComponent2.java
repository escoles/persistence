package cat.jaumemoron.centers.persistence.component;

import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.dto.AppointmentDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

@Component
public class UstecComponent2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(UstecComponent2.class);

    @Value("${ustec.connection.newUrl}")
    private String userUrl;

    @Value("${ustec.date.format}")
    private String dateFormat;

    @Value("${ustec.date.query}")
    private String dateQuery;

    @Value("${ustec.file.repository}")
    private String repository;

    private final RestTemplate restTemplate = new RestTemplate();

    public File getAppointments(Date localDate, String target) {
        if (localDate == null)
            throw new IllegalArgumentException("Parameter date cannot be null");
        SimpleDateFormat csvDf = new SimpleDateFormat(PersistenceConstants.CSV_DATE_FORMAT);
        String filename = csvDf.format(localDate) + ".appointment";

        SimpleDateFormat sdfQuery = new SimpleDateFormat(dateQuery);
        String dateQuery = sdfQuery.format(localDate);

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String date = sdf.format(localDate);
        String url;
        if (ObjectUtils.isEmpty(target))
            url = userUrl.replace("$DATE", dateQuery);
        else
            url = target;
        LOGGER.debug("Connecting to [{}]...", url);
        File file = null;
        try {
            MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
            converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
            restTemplate.getMessageConverters().add(converter);
            HttpHeaders headers = new HttpHeaders();
            HttpEntity<String> requestEntity = new HttpEntity<>(headers);
            ResponseEntity<UstecData> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, UstecData.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                UstecData data = response.getBody();
                if (!ObjectUtils.isEmpty(data) && !ObjectUtils.isEmpty(data.getData())) {
                    Path path = Paths.get(repository);
                    Path filenamePath = Paths.get(repository + "/" + filename);
                    file = filenamePath.toFile();
                    if (!path.toFile().exists())
                        Files.createDirectory(path);
                    if (file.exists()) {
                        Files.delete(filenamePath);
                    }
                    file = Files.createFile(filenamePath).toFile();
                    try (FileWriter writer = new FileWriter(file)) {
                        // Escribim la capçalera
                        String header = PersistenceConstants.CSV_TERRITORIAL_FIELD + "," +
                                PersistenceConstants.CSV_DATE_FIELD + "," +
                                PersistenceConstants.CSV_NUMBER_FIELD + "," +
                                PersistenceConstants.CSV_CENTER_FIELD + "," +
                                PersistenceConstants.CSV_DAYTYPE_FIELD + "," +
                                PersistenceConstants.CSV_SPECIALITY_FIELD + "," +
                                PersistenceConstants.CSV_INI_DATE_FIELD + "," +
                                PersistenceConstants.CSV_END_DATE_FIELD + ",";
                        writer.write(header);
                        for (var record : data.getData()) {
                            AppointmentDTO dto = processElement(record);
                            if (dto != null) {
                                dto.setDate(date);
                                writer.write("\n");
                                writer.write(dto.toString());
                            }
                        }

                    }
                } else {
                    LOGGER.error("Resposta buida d'Ustec");
                }
            } else {
                LOGGER.error("Resposta errònia d'Ustec: {}", response.getStatusCode());
            }
        } catch (Exception e) {
            LOGGER.error("Error en la sol·licitud a 'ustec.com': {}", e.getMessage());
        }
        return file;
    }

    private AppointmentDTO processElement(UstecData.DataItem item) {
        // Ignorarem si es la capçalera
        AppointmentDTO appointment = new AppointmentDTO();
        String territorial = null;
        var value = item.getTerritorialName()!=null?item.getTerritorialName().toLowerCase():"";
        if (value.contains("consorci")) {
            territorial = "1";
        }
        if (value.contains("comarques")) {
            territorial = "2";
        }
        if (value.contains("llobregat")) {
            territorial = "3";
        }
        if (value.contains("occidental")) {
            territorial = "4";
        }
        if (value.contains("oriental")) {
            territorial = "5";
        }
        if (value.contains("central")) {
            territorial = "6";
        }
        if (value.contains("girona")) {
            territorial = "17";
        }
        if (value.contains("lleida")) {
            territorial = "25";
        }
        if (value.contains("tarragona")) {
            territorial = "43";
        }
        if (value.contains("ebre")) {
            territorial = "44";
        }
        appointment.setTerritorial(territorial);
        appointment.setNumber(item.getNumber().equals("No adjudicat")?null:item.getNumber());
        appointment.setCenter(item.getCenter());
        appointment.setDayType(item.getDayType());
        appointment.setInit(getDateFormat(item.getInit()));
        appointment.setEnd(getDateFormat(item.getEnd()));
        appointment.setSpeciality(item.getSpeciality());
        // Si no tenim territorial ni especialitat considerem és un registre en blanc
        if (ObjectUtils.isEmpty(appointment.getNumber()) || ObjectUtils.isEmpty(appointment.getTerritorial()) || ObjectUtils.isEmpty(appointment.getSpeciality()))
            return null;
        return appointment;
    }

    private String getDateFormat(String value) {
        String retval = value;
        // Comprobem el cas que l'any tingui només dos dígits
        if (retval.length() == 8) {
            int index = retval.lastIndexOf('/');
            // Si la segona '/' és a la posició 5 vol dir que el dia i mes tenen format correcte
            if (index == 5) {
                retval = retval.substring(0, index + 1) + "20" + retval.substring(index + 1);
            }
        }
        return retval;
    }
}
