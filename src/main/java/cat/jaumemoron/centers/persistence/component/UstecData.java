package cat.jaumemoron.centers.persistence.component;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UstecData {

    private List<DataItem> data;

    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Getter
    public static class DataItem {
        @JsonProperty("0")
        private String territorialName;
        @JsonProperty("2")
        private String number;
        @JsonProperty("3")
        private String center;
        @JsonProperty("4")
        private String dayType;
        @JsonProperty("5")
        private String speciality;
        @JsonProperty("6")
        private String init;
        @JsonProperty("7")
        private String end;

    }

}
