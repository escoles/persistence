package cat.jaumemoron.centers.persistence;

import cat.jaumemoron.centers.persistence.domain.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DictionaryTest {

    @Test
    public void dictionaryClasses() {
        assertEquals(City.of("1", "City"), City.of("1", "City"));
        assertEquals(Region.of("2", "Region"), Region.of("2", "Region"));
        assertEquals(Territorial.of("3", "Territorial"), Territorial.of("3", "Territorial"));
        assertEquals(Nature.of("4", "Nature"), Nature.of("4", "Nature"));
        assertEquals(Owner.of("5", "Owner"), Owner.of("5", "Owner"));
    }
}
