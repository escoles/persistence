package cat.jaumemoron.centers.persistence.importer;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class CenterFileImporterTest {

    @Autowired
    private CenterFileImporter importer;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void importFile() throws IOException, ParseException {
        importFile(this.getClass().getResource("/files/center_file.csv").getPath());
    }

    @Test
    public void importFails() throws IOException, ParseException {
        importFails(this.getClass().getResource("/files/center_file_with_errors.csv").getPath());
    }

    @Test
    public void importFails1() {
        try {
            importer.importFile(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void importFails2() {
        try {
            importer.importFile("pp");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void importFails3() {
        try {
            importer.importFile("/tmp");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    private void importFile(String filename) throws IOException, ParseException {
        assertNotNull(filename);
        GenericImporterResult result = importer.importFile(filename);
        assertFalse(result.hasErrors());
        assertFalse(((CenterImporterResult) result).getCityList().isEmpty());
        assertFalse(((CenterImporterResult) result).getRegionList().isEmpty());
        assertFalse(((CenterImporterResult) result).getNatureList().isEmpty());
        assertFalse(((CenterImporterResult) result).getOwnerList().isEmpty());
    }

    private void importFails(String filename) throws IOException, ParseException {
        assertNotNull(filename);
        GenericImporterResult result = importer.importFile(filename);
        assertTrue(result.hasErrors());
    }

}
