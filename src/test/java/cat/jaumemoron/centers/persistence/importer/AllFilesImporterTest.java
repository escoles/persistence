package cat.jaumemoron.centers.persistence.importer;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AllFilesImporterTest {

    @Autowired
    private CenterFileImporter centerImporter;

    @Autowired
    private AppointmentFileImporter appointmentImporter;

    @Autowired
    private SpecialityFileImporter appointmentSpecialityImporter;

    //    @Test
    public void importAll() throws IOException, ParseException {

        System.out.println("Loading Specialities...");
        importFile(this.getClass().getResource("/files/conf/specialities.csv").getPath(), appointmentSpecialityImporter);

        System.out.println("Loading centers...");
        importFile(this.getClass().getResource("/files/centers/baix_llobregat.csv").getPath(), centerImporter);
        importFile(this.getClass().getResource("/files/centers/catalunya_central.csv").getPath(), centerImporter);
        importFile(this.getClass().getResource("/files/centers/consorci_educacio_de_barcelona.csv").getPath(), centerImporter);
        importFile(this.getClass().getResource("/files/centers/girona.csv").getPath(), centerImporter);
        importFile(this.getClass().getResource("/files/centers/lleida.csv").getPath(), centerImporter);
        importFile(this.getClass().getResource("/files/centers/maresme-valles_oriental.csv").getPath(), centerImporter);
        importFile(this.getClass().getResource("/files/centers/tarragona.csv").getPath(), centerImporter);
        importFile(this.getClass().getResource("/files/centers/terres_de_l_ebre.csv").getPath(), centerImporter);
        importFile(this.getClass().getResource("/files/centers/totcat_barcelona_comarques.csv").getPath(), centerImporter);
        importFile(this.getClass().getResource("/files/centers/valles_occidental.csv").getPath(), centerImporter);
        System.out.println("Loading Appointments...");
        importFile(this.getClass().getResource("/files/appointments1.csv").getPath(), appointmentImporter);
        importFile(this.getClass().getResource("/files/appointments2.csv").getPath(), appointmentImporter);
    }


    private void importFile(String filename, GenericImporter importer) throws IOException, ParseException {
        assertNotNull(filename);
        importer.importFile(filename);
    }

}
