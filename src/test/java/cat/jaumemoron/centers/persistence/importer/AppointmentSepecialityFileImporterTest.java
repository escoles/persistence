package cat.jaumemoron.centers.persistence.importer;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class AppointmentSepecialityFileImporterTest {

    @Autowired
    private SpecialityFileImporter importer;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void export() throws IOException, ParseException {
        String filename = this.getClass().getResource("/files/conf/specialities.csv").getPath();
        assertNotNull(filename);
        GenericImporterResult result = importer.importFile(filename);
        assertFalse(result.hasErrors());
    }

}
