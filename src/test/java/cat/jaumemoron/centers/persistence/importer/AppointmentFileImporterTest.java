package cat.jaumemoron.centers.persistence.importer;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.domain.Territorial;
import cat.jaumemoron.centers.persistence.service.TerritorialService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class AppointmentFileImporterTest {

    @Autowired
    private AppointmentFileImporter importer;

    @Autowired
    private TerritorialService territorialService;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void export() throws IOException, ParseException {
        String filename = this.getClass().getResource("/files/appointments_file.csv").getPath();
        assertNotNull(filename);
        GenericImporterResult result = importer.importFile(filename);
        // Conté només errors de centres no trobats
        assertTrue(result.hasErrors());
        assertEquals(result.getErrors().size(), 1);
        List<Territorial> list = territorialService.findAll();
        assertNotNull(list);
        assertFalse(list.isEmpty());
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void exportWithErrors() throws IOException, ParseException {
        String filename = this.getClass().getResource("/files/appointments_file_with_errors.csv").getPath();
        assertNotNull(filename);
        GenericImporterResult result = importer.importFile(filename);
        assertTrue(result.hasErrors());
        assertEquals(result.getErrors().size(), 10);
    }

    public void exportAll() throws IOException, ParseException {
        importer.importFile(this.getClass().getResource("/files/appointments1.csv").getPath());
    }
}
