package cat.jaumemoron.centers.persistence;


import cat.jaumemoron.centers.persistence.configuration.EnablePersistenceConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnablePersistenceConfiguration
public class PersistenceSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersistenceSpringBootApplication.class, args);
    }


}
