package cat.jaumemoron.centers.persistence.event;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.constants.StatisticType;
import cat.jaumemoron.centers.persistence.domain.Statistic;
import cat.jaumemoron.centers.persistence.service.StatisticsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class PersistenceEventListenerTest {

    @Autowired
    private PersistenceEventListener listener;

    @Autowired
    private StatisticsService service;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void handleTelegramRequestEvent() {
        // Comprobem que no hi ha cap estadística creada
        PageRequest request = PageRequest.of(0, 5);
        Page<Statistic> page = service.findByType(StatisticType.TELEGRAM_REQUEST, request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 0L);

        TelegramRequestEvent event1 = new TelegramRequestEvent(true);
        TelegramRequestEvent event2 = new TelegramRequestEvent(false);
        TelegramRequestEvent event3 = new TelegramRequestEvent(true);

        // Processem event 1
        listener.handle(event1);
        page = service.findByType(StatisticType.TELEGRAM_REQUEST, request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 1L);
        assertEquals(page.getContent().get(0).getNumberOfOks(), 1L);
        assertEquals(page.getContent().get(0).getNumberOfErrors(), 0L);

        // Processem event 2
        listener.handle(event2);
        page = service.findByType(StatisticType.TELEGRAM_REQUEST, request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 1L);
        assertEquals(page.getContent().get(0).getNumberOfOks(), 1L);
        assertEquals(page.getContent().get(0).getNumberOfErrors(), 1L);

        // Processem event 3
        listener.handle(event3);
        page = service.findByType(StatisticType.TELEGRAM_REQUEST, request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 1L);
        assertEquals(page.getContent().get(0).getNumberOfOks(), 2L);
        assertEquals(page.getContent().get(0).getNumberOfErrors(), 1L);

        // Esborrem els registres creats
        service.remove(StatisticType.TELEGRAM_REQUEST);
        page = service.findByType(StatisticType.TELEGRAM_REQUEST, request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 0L);
    }
}
