package cat.jaumemoron.centers.persistence;

import cat.jaumemoron.centers.persistence.configuration.EnablePersistenceConfiguration;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.utils.SecurityUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootApplication
@EnablePersistenceConfiguration
public class SecurityUtilsTest {

    @Test
    public void test() {
        assertNull(SecurityContextHolder.getContext().getAuthentication());
        SecurityUtils.initializeSecurityContextAsAdmin();
        assertNotNull(SecurityContextHolder.getContext().getAuthentication());
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        assertEquals(user.getUsername(), PersistenceConstants.ADMIN_USERNAME);
    }
}
