package cat.jaumemoron.centers.persistence.component;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.importer.AppointmentFileImporter;
import cat.jaumemoron.centers.persistence.importer.GenericImporterResult;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class UstecComponent2Test {

    private static final String DATE_LARGE = "12/06/2018";
    private static final String DATE_IGNORE = "13/06/2018";

    private static WireMockServer wireMockServer = new WireMockServer(9999);

    @AfterAll
    public static void stopJiraServer() {
        wireMockServer.stop();
    }

    @BeforeAll
    public static void configJiraServer() throws URISyntaxException, IOException {
        byte[] appointmentsFile = Files.readAllBytes(Paths.get(UstecComponent2Test.class.getResource("/files/appointments.json").toURI()));
        wireMockServer.start();
        wireMockServer.stubFor(
                get(urlEqualTo("/nomenaments/nous"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "text/html; charset=UTF-8")
                                .withBody(new String(appointmentsFile))));
    }

    @Autowired
    private UstecComponent2 component;

    @Autowired
    private AppointmentFileImporter importer;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void getAppointments() throws IOException {
        File file = component.getAppointments(new Date(), null);
        assertTrue(file.exists());
        assertFalse(file.isDirectory());
        GenericImporterResult result = importer.importFile(file.getAbsolutePath());
        assertTrue(result.getRecordsNumber() > 10L);
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void getAppointments1() throws IOException {
        String url = "http://localhost:9999/nomenaments/nous";
        File file = component.getAppointments(new Date(), url);
        assertTrue(file.exists());
        assertFalse(file.isDirectory());
        GenericImporterResult result = importer.importFile(file.getAbsolutePath());
        assertTrue(result.getRecordsNumber() > 10L);
    }


}