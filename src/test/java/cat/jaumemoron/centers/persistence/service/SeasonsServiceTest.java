package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.domain.Season;
import cat.jaumemoron.centers.persistence.repository.SeasonsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class SeasonsServiceTest {

    @Autowired
    private SeasonsService service;

    @Autowired
    private SeasonsRepository repository;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void listAndCreate() {
        List<Season> seasonList = service.findAll();
        assertFalse(seasonList.isEmpty());
        Season s = new Season();
        s.setId("1999-2000");
        s = service.create(s);
        assertTrue(service.findAll().contains(s));
        Optional<Season> optional = service.findById(s.getId());
        assertTrue(optional.isPresent());
        assertEquals(optional.get(), s);
        repository.delete(s);
    }

}
