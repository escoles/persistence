package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.domain.Speciality;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class SpecialitiesServiceTest {

    @Autowired
    private SpecialitiesService service;

    @Test
    public void findAll() {
        List<Speciality> SpecialityList = service.findAll();
        assertFalse(SpecialityList.isEmpty());
    }

    @Test
    public void findById() {
        String id = "133";
        Optional<Speciality> optional = service.findById(id);
        assertTrue(optional.isPresent());
        optional = service.findById("not_exists");
        assertFalse(optional.isPresent());
    }

    @Test
    public void findByIdIsNull() {
        try {
            service.findById(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

}
