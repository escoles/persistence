package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.domain.Action;
import cat.jaumemoron.centers.persistence.event.ActionEvent;
import cat.jaumemoron.centers.persistence.event.PersistenceEventListener;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class ActionServiceTest {

    @Autowired
    private ActionService service;

    @Autowired
    private PersistenceEventListener listener;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void testCrudMethods() {
        PageRequest request = PageRequest.of(0, 10);
        // Obtenim la llista d'elements actual
        Page<Action> page = service.findAll(request);
        assertNotNull(page);
        long totalElements = page.getTotalElements();
        String name = "action-Name";
        String comment = "comment";
        Date date = new Date();
        // Creem l'entitat a bdd
        Action action = Action.of(name, date);
        action.addComment(comment);
        action = service.create(action);
        assertNotNull(action);
        assertNotNull(action.getId());
        assertEquals(name, action.getText());
        assertEquals(date, action.getDate());
        assertFalse(action.isRead());
        assertFalse(action.getComments().isEmpty());
        assertEquals(action.getComments().get(0), comment);

        // Comprobem que al llistat ens retorna un element més
        page = service.findAll(request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), totalElements + 1);

        page = service.findByTerm("name", request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 1L);

        page = service.findByTerm("noName", request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 0L);

        // Obtenim quans elements hi ha sense llegir
        long read = service.countUnRead();

        // Marquem l'element com a llegit
        service.markAsRead(action.getId());

        // Hem de tenir un element menys com a llegit
        assertEquals(service.countUnRead(), read - 1);

        // Obtenim el registre
        Optional<Action> optional = service.findById(action.getId());
        assertTrue(optional.isPresent());
        assertTrue(optional.get().isRead());

        // Eliminem el registre
        service.delete(action.getId());

        // Validem que el registre no existeix
        optional = service.findById(action.getId());
        assertFalse(optional.isPresent());

        // Comprobem que en el llistat ja no hi apareix
        page = service.findAll(request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), totalElements);
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void handleNewActionEvent() {
        PageRequest request = PageRequest.of(0, 10);
        // Obtenim la llista d'elements actual
        Page<Action> page = service.findAll(request);
        assertNotNull(page);
        long totalElements = page.getTotalElements();
        ActionEvent event = new ActionEvent("text");
        listener.handleNewActionEvent(event);
        // Comprobem que al llistat ens retorna un element més
        page = service.findAll(request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), totalElements + 1);
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void handleNewActionEventNoProcess() {
        listener.handleNewActionEvent(null);
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void handleNewActionEventNoProcess1() {
        listener.handleNewActionEvent(new ActionEvent((String) null));
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void createFails() {
        try {
            service.create(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void markAsReadFails() {
        try {
            service.markAsRead(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void markAsReadFails1() {
        try {
            service.markAsRead("");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void markAsReadNotFound() {
        service.markAsRead("idNotFound");
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void deleteFails() {
        try {
            service.delete(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void deleteFails1() {
        try {
            service.delete("");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findAll() {
        try {
            service.findAll(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }
}
