package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.domain.Owner;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class OwnerServiceTest {

    @Autowired
    private OwnerService service;

    @Test
    public void findAll() {
        List<Owner> list = service.findAll();
        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertTrue(list.size() > 3);
    }
}
