package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.constants.StatisticType;
import cat.jaumemoron.centers.persistence.domain.Statistic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class StatisticsServiceTest {

    @Autowired
    private StatisticsService service;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void testCrudMethods() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Statistic s1 = new Statistic();
        s1.setDate(sdf.parse("02/01/2018"));
        s1.setType(StatisticType.TELEGRAM_REQUEST);
        s1.setNumberOfOks(1L);
        s1.setNumberOfErrors(0L);
        Statistic s2 = new Statistic();
        s2.setDate(sdf.parse("05/01/2018"));
        s2.setType(StatisticType.TELEGRAM_REQUEST);
        s2.setNumberOfOks(0L);
        s2.setNumberOfErrors(1L);

        s1 = service.save(s1);
        s2 = service.save(s2);

        Optional<Statistic> optional = service.findById(s1.getDate());
        assertTrue(optional.isPresent());

        PageRequest request = PageRequest.of(0, 5);

        Page<Statistic> page = service.findByType(StatisticType.TELEGRAM_REQUEST, request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 2);

        page = service.findByType(StatisticType.TELEGRAM_REQUEST, sdf.parse("01/01/2018"), sdf.parse("01/02/2018"), request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 2);

        page = service.findByType(StatisticType.TELEGRAM_REQUEST, sdf.parse("01/01/2018"), sdf.parse("04/01/2018"), request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 1);

        service.remove(s1.getDate());
        page = service.findByType(StatisticType.TELEGRAM_REQUEST, request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 1);

        service.remove(s2.getType());

        page = service.findByType(StatisticType.TELEGRAM_REQUEST, request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() == 0);

    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void saveFails() {
        try {
            service.save(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findByIdFails() {
        try {
            service.findById(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findByTypeFails1() {
        try {
            service.findByType(null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findByTypeFails2() {
        try {
            service.findByType(StatisticType.TELEGRAM_REQUEST, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findByTypeFails3() {
        try {
            service.findByType(StatisticType.TELEGRAM_REQUEST, null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findByTypeFails4() {
        try {
            service.findByType(StatisticType.TELEGRAM_REQUEST, new Date(), null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void findByTypeFails5() {
        try {
            service.findByType(StatisticType.TELEGRAM_REQUEST, new Date(), new Date(), null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

}
