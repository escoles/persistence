package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.domain.Territorial;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class TerritorialServiceTest {

    @Autowired
    private TerritorialService service;

    @Test
    public void findAll() {
        List<Territorial> list = service.findAll();
        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertTrue(list.size() > 3);
    }

    @Test
    public void findByIdFails() {
        try {
            service.findById(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByIdFails1() {
        try {
            service.findById("");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByAppointmentId() {
        String appointmentId = "43";
        Optional<Territorial> optional = service.findByAppointmentId(appointmentId);
        assertTrue(optional.isPresent());
        assertEquals(optional.get().getId(), "0143");
    }

    @Test
    public void findByAppointmentIdFails() {
        try {
            service.findByAppointmentId(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByAppointmentIdFails1() {
        try {
            service.findByAppointmentId("");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }
}
