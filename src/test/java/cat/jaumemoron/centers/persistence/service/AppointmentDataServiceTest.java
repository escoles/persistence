package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.domain.*;
import cat.jaumemoron.centers.persistence.repository.AppointmentDataDayTypeRepository;
import cat.jaumemoron.centers.persistence.repository.AppointmentDataRepository;
import cat.jaumemoron.centers.persistence.service.impl.AppointmentDataServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.SearchHitsImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class AppointmentDataServiceTest {

    @Autowired
    private AppointmentDataServiceImpl service;

    @Test
    public void findBySeasonAndTerritorial() {
        // Configuring mocks
        List<AppointmentData> list = new ArrayList<>();
        list.add(new AppointmentData());
        list.add(new AppointmentData());
        list.add(new AppointmentData());
        Page<AppointmentData> pageMocked = new PageImpl<>(list);
        var appointmentDataRepository = Mockito.mock(AppointmentDataRepository.class);
        var appointmentDataDayTypeRepository = Mockito.mock(AppointmentDataDayTypeRepository.class);
        when(appointmentDataRepository.findBySeasonAndTerritorialId(ArgumentMatchers.any(), ArgumentMatchers.any(),
                ArgumentMatchers.any())).thenReturn(pageMocked);
        List<AppointmentDataDayType> list2 = new ArrayList<>();
        list2.add(new AppointmentDataDayType());
        Page<AppointmentDataDayType> pageMocked2 = new PageImpl<>(list2);
        when(appointmentDataDayTypeRepository.findBySeasonAndTerritorialIdAndDayType(ArgumentMatchers.any(),
                ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(pageMocked2);
        ReflectionTestUtils.setField(service, "appointmentDataRepository", appointmentDataRepository);
        ReflectionTestUtils.setField(service, "appointmentDataDayTypeRepository", appointmentDataDayTypeRepository);
        // Testing methods
        String season = "2000-2001";
        String territorialId = "0308";
        PageRequest request = PageRequest.of(0, 5);
        Page<AppointmentData> page = service.findBySeasonAndTerritorial(season, territorialId, request);
        assertNotNull(page);
        assertEquals(3L, page.getTotalElements());
        Page<AppointmentDataDayType> page2 = service.findBySeasonAndTerritorialIdAndDayType(season, territorialId, DayType.FULL, request);
        assertNotNull(page2);
        assertEquals(1L, page2.getTotalElements());
    }

    @Test
    public void findBySeasonAndTerritorialAndSpeciality() {
        // Configuring mocks
        List<SearchHit> list = new ArrayList<>();
        SearchHits hits = new SearchHitsImpl(1, null, 0, null, list, null);
        ElasticsearchOperations operations = Mockito.mock(ElasticsearchOperations.class);
        when(operations.search(ArgumentMatchers.any(NativeSearchQuery.class), ArgumentMatchers.any())).thenReturn(hits);
        // Testing methods
        String season = "2000-2001";
        String territorialId = "0308";
        String speciality = "133";
        PageRequest request = PageRequest.of(0, 5);
        Page<AppointmentData> page = service.findBySeasonAndTerritorialAndSpeciality(season, territorialId, speciality, request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 2);
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAndDayType() {
        // Configuring mocks
        List<SearchHit> list = new ArrayList<>();
        SearchHits hits = new SearchHitsImpl(1, null, 0, null, list, null);
        ElasticsearchOperations operations = Mockito.mock(ElasticsearchOperations.class);
        when(operations.search(ArgumentMatchers.any(NativeSearchQuery.class), ArgumentMatchers.any())).thenReturn(hits);
        // Testing methods
        String season = "2000-2001";
        String territorialId = "0308";
        String speciality = "133";
        PageRequest request = PageRequest.of(0, 5);
        Page<AppointmentDataDayType> page = service.findBySeasonAndTerritorialAndSpecialityAndDayType(season, territorialId, speciality, DayType.FULL, request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 1);
        page = service.findBySeasonAndTerritorialAndSpecialityAndDayType(season, territorialId, speciality, DayType.A_THIRD, request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 1);
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAllDayType() {
        // Configuring mocks
        List<SearchHit> list = new ArrayList<>();
        SearchHits hits = new SearchHitsImpl(1, null, 0, null, list, null);
        ElasticsearchOperations operations = Mockito.mock(ElasticsearchOperations.class);
        when(operations.search(ArgumentMatchers.any(NativeSearchQuery.class), ArgumentMatchers.any())).thenReturn(hits);
        // Testing methods
        String season = "2000-2001";
        String territorialId = "0308";
        String speciality = "133";
        PageRequest request = PageRequest.of(0, 5);
        Page<AppointmentDataDayType> page = service.findBySeasonAndTerritorialAndSpecialityAllDayType(season, territorialId, speciality, request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 2);
    }

    @Test
    public void findBySeasonAndTerritorialIdAndDayTypeAll() {
        // Configuring mocks
        List<AppointmentDataDayType> list = new ArrayList<>();
        list.add(new AppointmentDataDayType());
        list.add(new AppointmentDataDayType());
        list.add(new AppointmentDataDayType());
        Page<AppointmentDataDayType> pageMocked2 = new PageImpl<>(list);
        var appointmentDataDayTypeRepository = Mockito.mock(AppointmentDataDayTypeRepository.class);
        when(appointmentDataDayTypeRepository.findBySeasonAndTerritorialId(ArgumentMatchers.any(),
                ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(pageMocked2);
        ReflectionTestUtils.setField(service, "appointmentDataDayTypeRepository", appointmentDataDayTypeRepository);
        // Testing methods
        String season = "2000-2001";
        String territorialId = "0308";
        PageRequest request = PageRequest.of(0, 5);
        Page<AppointmentDataDayType> page = service.findBySeasonAndTerritorialIdAndDayTypeAll(season, territorialId, request);
        assertNotNull(page);
        assertEquals(3, page.getTotalElements());
    }

    @Test
    public void findBySeasonAndTerritorialFails() {
        try {
            service.findBySeasonAndTerritorial(null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialFails1() {
        try {
            service.findBySeasonAndTerritorial("XX", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialFails2() {
        try {
            service.findBySeasonAndTerritorial("XX", "XX", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialIdAndDayTypeFails() {
        try {
            service.findBySeasonAndTerritorialIdAndDayType(null, null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialIdAndDayTypeFails1() {
        try {
            service.findBySeasonAndTerritorialIdAndDayType("XX", null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialIdAndDayTypeFails2() {
        try {
            service.findBySeasonAndTerritorialIdAndDayType("XX", "XX", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialIdAndDayTypeFails3() {
        try {
            service.findBySeasonAndTerritorialIdAndDayType("XX", "XX", DayType.FULL, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialIdAndDayTypeAllFails() {
        try {
            service.findBySeasonAndTerritorialIdAndDayTypeAll(null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialIdAndDayTypeAllFails1() {
        try {
            service.findBySeasonAndTerritorialIdAndDayTypeAll("XX", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialIdAndDayTypeAllFails2() {
        try {
            service.findBySeasonAndTerritorialIdAndDayTypeAll("XX", "XX", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityFails() {
        try {
            service.findBySeasonAndTerritorialAndSpeciality(null, null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityFails1() {
        try {
            service.findBySeasonAndTerritorialAndSpeciality("XX", null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityFails2() {
        try {
            service.findBySeasonAndTerritorialAndSpeciality("XX", "XX", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityFails3() {
        try {
            service.findBySeasonAndTerritorialAndSpeciality("XX", "XX", "XX", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAndDayTypeFails1() {
        try {
            service.findBySeasonAndTerritorialAndSpecialityAndDayType(null, null, null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAndDayTypeFails2() {
        try {
            service.findBySeasonAndTerritorialAndSpecialityAndDayType("XX", null, null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAndDayTypeFails3() {
        try {
            service.findBySeasonAndTerritorialAndSpecialityAndDayType("XX", "XX", null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAndDayTypeFails4() {
        try {
            service.findBySeasonAndTerritorialAndSpecialityAndDayType("XX", "XX", "XX", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAndDayTypeFails5() {
        try {
            service.findBySeasonAndTerritorialAndSpecialityAndDayType("XX", "XX", "XX", DayType.FULL, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAllDayTypeFails() {
        try {
            service.findBySeasonAndTerritorialAndSpecialityAllDayType(null, null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAllDayTypeFails1() {
        try {
            service.findBySeasonAndTerritorialAndSpecialityAllDayType("XX", null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAllDayTypeFails2() {
        try {
            service.findBySeasonAndTerritorialAndSpecialityAllDayType("XX", "XX", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findBySeasonAndTerritorialAndSpecialityAllDayTypeFails3() {
        try {
            service.findBySeasonAndTerritorialAndSpecialityAllDayType("XX", "XX", "XX", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void update() {
        // Configuring mocks
        var appointmentDataRepository = Mockito.mock(AppointmentDataRepository.class);
        var appointmentDataDayTypeRepository = Mockito.mock(AppointmentDataDayTypeRepository.class);
        doNothing().when(appointmentDataRepository).deleteAll();
        doNothing().when(appointmentDataDayTypeRepository).deleteAll();
        List<Season> seasonList = new ArrayList<>();
        seasonList.add(Season.of("2019-2020"));
        var seasonsService = Mockito.mock(SeasonsService.class);
        when(seasonsService.findAll()).thenReturn(seasonList);
        List<AppointmentData> appointmentDataList = new ArrayList<>();
        Date date = new Date();
        long number = 100;
        appointmentDataList.add(AppointmentData.Builder.anAppointmentData().withSeason("2019-2020")
                .withData(AppointmentNumberData.of(number, date))
                .withSpeciality(AppointmentSpeciality.of("1", "1"))
                .withTerritorial(AppointmentTerritorial.of("1", "1")).build());
        var appointmentService = Mockito.mock(AppointmentService.class);
        when(appointmentService.getMaxAppointmentNumber(ArgumentMatchers.any())).thenReturn(appointmentDataList);
        when(appointmentService.getMaxAppointmentNumberFromLastDate(ArgumentMatchers.any())).thenReturn(appointmentDataList);
        List<AppointmentDataDayType> appointmentDataDayTypeList = new ArrayList<>();
        appointmentDataDayTypeList.add(new AppointmentDataDayType());
        when(appointmentService.getAppointmentDataGroupByTerritorialAndSpecialityAndDayType(ArgumentMatchers.any())).thenReturn(appointmentDataDayTypeList);
        when(appointmentDataRepository.saveAll(ArgumentMatchers.any())).thenReturn(null);
        when(appointmentDataDayTypeRepository.saveAll(ArgumentMatchers.any())).thenReturn(null);
        ReflectionTestUtils.setField(service, "appointmentDataRepository", appointmentDataRepository);
        ReflectionTestUtils.setField(service, "appointmentDataDayTypeRepository", appointmentDataDayTypeRepository);
        ReflectionTestUtils.setField(service, "seasonsService", seasonsService);
        ReflectionTestUtils.setField(service, "appointmentService", appointmentService);
        // Testing methods
        service.update();
    }

}
