package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.constants.DayType;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.domain.*;
import cat.jaumemoron.centers.persistence.dto.NumberDataDTO;
import cat.jaumemoron.centers.persistence.dto.TerritorialCountDTO;
import cat.jaumemoron.centers.persistence.repository.AppointmentsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class AppointmentServiceTest {

    @Autowired
    private AppointmentsRepository repository;

    @Autowired
    private AppointmentService service;

    @Autowired
    private TerritorialService territorialsService;

    @Autowired
    private SpecialitiesService specialitiesService;

    @Autowired
    private SeasonsService seasonsService;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void save() {
        Territorial territorial = territorialsService.findAll().get(0);
        Speciality speciality = specialitiesService.findAll().get(0);
        Season season = seasonsService.findAll().get(0);
        PageRequest request = PageRequest.of(0, 4);
        Page<Appointment> page = service.findByTerritorial(season.getId(), territorial.getId(), request);
        assertNotNull(page);
        long total = page.getTotalElements();
        int numberOrElements = page.getNumberOfElements();
        Appointment appointment = new Appointment();
        appointment.setId("1");
        appointment.setDayType(DayType.FULL);
        appointment.setCenterName("centerName");
        appointment.setNumber(1L);
        appointment.setTerritorial(territorial);
        appointment.setSpeciality(speciality);
        appointment.setDate(new Date());
        appointment.setInit(new Date());
        appointment.setEnd(new Date());
        appointment.setSeason(season);
        appointment = service.save(appointment);
        Optional<Appointment> optional = service.findById(appointment.getId());
        assertTrue(optional.isPresent());
        assertEquals(optional.get().getCenterName(), appointment.getCenterName());
        assertEquals(optional.get().getTerritorial(), appointment.getTerritorial());
        assertEquals(optional.get().getSpeciality(), appointment.getSpeciality());
        assertEquals(optional.get().getDayType(), appointment.getDayType());
        page = service.findByTerritorial(season.getId(), territorial.getId(), request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), total + 1);
        assertEquals(page.getNumberOfElements(), numberOrElements + 1);
        List<AppointmentData> data = service.getMaxAppointmentNumber(season.getId());
        assertNotNull(data);
        assertFalse(data.isEmpty());
        assertEquals(data.get(1).getSeason(), appointment.getSeason().getId());
        assertEquals(data.get(1).getTerritorial().getId(), appointment.getTerritorial().getId());
        assertEquals(data.get(1).getSpeciality().getId(), appointment.getSpeciality().getId());
        assertEquals(data.get(1).getData().getMaxNumber(), appointment.getNumber());
        data = service.getMaxAppointmentNumberFromLastDate(season.getId());
        assertNotNull(data);
        assertFalse(data.isEmpty());
        assertEquals(data.get(1).getSeason(), appointment.getSeason().getId());
        assertEquals(data.get(1).getTerritorial().getId(), appointment.getTerritorial().getId());
        assertEquals(data.get(1).getSpeciality().getId(), appointment.getSpeciality().getId());
        assertEquals(data.get(1).getData().getMaxNumber(), appointment.getNumber());
        List<AppointmentDataDayType> data2 = service.getAppointmentDataGroupByTerritorialAndSpecialityAndDayType(season.getId());
        assertNotNull(data);
        assertFalse(data2.isEmpty());
        assertEquals(data2.get(1).getSeason(), appointment.getSeason().getId());
        assertEquals(data2.get(1).getTerritorial().getId(), appointment.getTerritorial().getId());
        assertEquals(data2.get(1).getSpeciality().getId(), appointment.getSpeciality().getId());
        assertEquals(data2.get(1).getData().getMaxNumber(), appointment.getNumber());
        assertEquals(data2.get(1).getDayType(), appointment.getDayType());
        page = service.findByTerritorialAndSpeciality(season.getId(), appointment.getTerritorial().getId(), speciality.getId(), request);
        assertFalse(page.getContent().isEmpty());
        assertEquals(page.getContent().size(), 1);
        long count = service.countBySeasonId(season.getId());
        assertEquals(count, 2L);
        repository.delete(appointment);
    }

    @Test
    public void findByTerritorialIdFails() {
        try {
            service.findByTerritorial(null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialIdFails1() {
        try {
            service.findByTerritorial(null, "1", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialIdFails2() {
        try {
            service.findByTerritorial("1", "1", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void saveFails1() {
        try {
            service.save(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialAndTermFails() {
        try {
            service.findByTerritorialAndSpeciality(null, null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialAndTermFails1() {
        try {
            service.findByTerritorialAndSpeciality("1", null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialAndTermFails2() {
        try {
            service.findByTerritorialAndSpeciality("1", "1", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialAndTermFails3() {
        try {
            service.findByTerritorialAndSpeciality("1", "1", "1", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByIdFails() {
        try {
            service.findById(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByIdFail1() {
        try {
            service.findById("");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void getAppointmentDataGroupByTerritorialAndSpecialityAndDayTypeFails() {
        try {
            service.getAppointmentDataGroupByTerritorialAndSpecialityAndDayType(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void getMaxAppointmentNumber() {
        try {
            service.getMaxAppointmentNumber(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void getMaxAppointmentNumberFromLastDate() {
        try {
            service.getMaxAppointmentNumberFromLastDate(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void countBySeasonIdFails() {
        try {
            service.countBySeasonId(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void getTerritorialsCountByDate() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        List<TerritorialCountDTO> list = service.getTerritorialsCountByDate(sdf.parse("23/07/2016"));
        assertNotNull(list);
        assertFalse(list.isEmpty());
        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getTerritorial().toString(), "0508 - Maresme - Vallès Oriental");
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void getTerritorialsCountByDateFails() {
        try {
            service.getTerritorialsCountByDate(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void getTerritorialsLastNumber() {
        List<TerritorialCountDTO> list = service.getTerritorialsLastNumber("2017-2018");
        assertNotNull(list);
        assertFalse(list.isEmpty());
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void getTerritorialsLastNumberFails() {
        try {
            service.getTerritorialsLastNumber(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void getNumberData() {
        String season = "2017-2018";
        String territorialId = "0508";
        String specialityId = "192";
        long number = 10033;
        long number1 = 10034;
        long number2 = 10100;
        long number3 = 998;
        NumberDataDTO data = service.getNumberData(season, territorialId, specialityId, number);
        assertNotNull(data);
        assertTrue(data.isFound());
        assertEquals(data.getAppointmentList().size(), 1);
        assertEquals(number, data.getAppointmentList().get(0).getNumber());
        data = service.getNumberData(season, territorialId, specialityId, number2);
        assertNotNull(data);
        assertFalse(data.isFound());
        assertEquals(season, data.getSeason());
        assertEquals(number1, data.getAppointmentList().get(0).getNumber());
        data = service.getNumberData(season, territorialId, specialityId, 100);
        assertNotNull(data);
        assertEquals(season, data.getSeason());
        assertEquals(number3, data.getAppointmentList().get(0).getNumber());
        String season2 = "2010-2011";
        data = service.getNumberData(season2, territorialId, specialityId, 100);
        assertNotNull(data);
        assertEquals(season2, data.getSeason());
        assertTrue(data.getAppointmentList().isEmpty());
    }

    @Test
    public void getNumberDataFails() {
        try {
            service.getNumberData(null, null, null, 0);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void getNumberDataFails1() {
        try {
            service.getNumberData("2017-2018", null, null, 0);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void getNumberDataFails2() {
        try {
            service.getNumberData("2017-2018", "0508", null, 0);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void getNumberDataFails3() {
        try {
            service.getNumberData("2017-2018", "0508", "192", 0);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }
}
