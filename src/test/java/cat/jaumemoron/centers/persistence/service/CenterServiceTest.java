package cat.jaumemoron.centers.persistence.service;

import cat.jaumemoron.centers.persistence.PersistenceApplicationTest;
import cat.jaumemoron.centers.persistence.TestUtils;
import cat.jaumemoron.centers.persistence.constants.PersistenceConstants;
import cat.jaumemoron.centers.persistence.domain.Center;
import cat.jaumemoron.centers.persistence.domain.City;
import cat.jaumemoron.centers.persistence.domain.Region;
import cat.jaumemoron.centers.persistence.domain.Type;
import cat.jaumemoron.centers.persistence.exception.DataRequiredException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = PersistenceApplicationTest.class)
public class CenterServiceTest {

    @Autowired
    private CenterService service;

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void createUpdateAndDelete() {
        String id = "1";
        Center center = TestUtils.getCenter();
        center.setId(id);
        service.save(center);
        Optional<Center> optional = service.findById(id);
        assertTrue(optional.isPresent());
        Center center2 = optional.get();
        assertEquals(center2.getId(), center.getId());
        assertEquals(center2.getName(), center.getName());
        assertEquals(center2.getTypeList(), center.getTypeList());
        assertEquals(center2.getAddress(), center.getAddress());
        assertEquals(center2.getPostalCode(), center.getPostalCode());
        assertEquals(center2.getCity(), center.getCity());
        assertEquals(center2.getRegion(), center.getRegion());
        assertEquals(center2.getFax(), center.getFax());
        assertFalse(center2.getEmailList().isEmpty());
        assertFalse(center2.getPhoneList().isEmpty());
        assertEquals(center2.getPhoneList().size(), 1);
        assertEquals(center2.getEmailList().size(), 2);
        assertEquals(center2.getLastModificationDate(), center.getLastModificationDate());
        center2.addTelefon("telefon2");
        center2.addEmail("email3");
        service.save(center2);
        Optional<Center> optional2 = service.findById(id);
        assertTrue(optional2.isPresent());
        Center center3 = optional2.get();
        assertEquals(center3.getId(), center2.getId());
        assertEquals(center3.getName(), center2.getName());
        assertEquals(center3.getTypeList(), center2.getTypeList());
        assertEquals(center3.getAddress(), center2.getAddress());
        assertEquals(center3.getPostalCode(), center2.getPostalCode());
        assertEquals(center3.getCity(), center2.getCity());
        assertEquals(center3.getRegion(), center2.getRegion());
        assertEquals(center3.getFax(), center2.getFax());
        assertFalse(center3.getEmailList().isEmpty());
        assertFalse(center3.getPhoneList().isEmpty());
        assertEquals(center3.getPhoneList().size(), 2);
        assertEquals(center3.getEmailList().size(), 3);
        assertEquals(center3.getLastModificationDate(), center2.getLastModificationDate());
        service.delete(center3);
        Optional<Center> optional3 = service.findById(id);
        assertFalse(optional3.isPresent());
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void createAndDeleteById() {
        String id = "1";
        Center centre = TestUtils.getCenter();
        centre.setId(id);
        service.save(centre);
        Optional<Center> optional = service.findById(id);
        assertTrue(optional.isPresent());
        service.deleteById(id);
        optional = service.findById(id);
        assertFalse(optional.isPresent());
    }

    @Test
    public void findByIdFails() {
        try {
            service.findById(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByIdFails2() {
        try {
            service.findById("");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findAll() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findAll(request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 6);
        assertEquals(page.getNumberOfElements(), 2);
        assertTrue(page.getTotalPages() >= 3);
    }

    @Test
    public void findAllFails() {
        try {
            service.findAll(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByType() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByType("EINF1C", request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 3);
        assertEquals(page.getNumberOfElements(), 2);
        assertTrue(page.getTotalPages() >= 2);
    }

    @Test
    public void findByTypeFails() {
        try {
            service.findByType(null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTypeFails1() {
        try {
            service.findByType("EINF1C", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByRegion() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByRegion("1", request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 4);
        assertEquals(page.getNumberOfElements(), 2);
        assertEquals(page.getTotalPages(), 2);
    }

    @Test
    public void findByRegionFails() {
        try {
            service.findByRegion(null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByRegionFails1() {
        try {
            service.findByRegion("region", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByRegionFails2() {
        try {
            service.findByRegion("", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByCity() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByCity("1", request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 4);
        assertEquals(page.getNumberOfElements(), 2);
        assertEquals(page.getTotalPages(), 2);
    }

    @Test
    public void findByCityFails() {
        try {
            service.findByCity(null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByCityFails1() {
        try {
            service.findByCity("city", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByCityFails2() {
        try {
            service.findByCity("", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTypeAndRegion() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByTypeAndRegion("EINF1C", "1", request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 2);
        assertEquals(page.getNumberOfElements(), 2);
        assertEquals(page.getTotalPages(), 1);
    }

    @Test
    public void findByTypeAndRegionFails() {
        try {
            service.findByTypeAndRegion(null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTypeAndRegionFails1() {
        try {
            service.findByTypeAndRegion("EINF1C", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTypeAndRegionFails2() {
        try {
            service.findByTypeAndRegion("EINF1C", "1", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTypeAndRegionFails3() {
        try {
            service.findByTypeAndRegion("EINF1C", "", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTypeAndCity() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByTypeAndCity("EINF1C", "1", request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 2);
        assertEquals(page.getNumberOfElements(), 2);
        assertEquals(page.getTotalPages(), 1);
    }

    @Test
    public void findByTypeAndCityFails() {
        try {
            service.findByTypeAndCity(null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTypeAndCityFails1() {
        try {
            service.findByTypeAndCity("EINF1C", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTypeAndCityFails2() {
        try {
            service.findByTypeAndCity("EINF1C", "1", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTypeAndCityFails3() {
        try {
            service.findByTypeAndCity("EINF1C", "", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByNameContaining() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByNameContaining("Maria", request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 2);
        assertEquals(page.getNumberOfElements(), 2);
        assertTrue(page.getTotalPages() >= 1);
    }

    @Test
    public void findByNameContainingFails() {
        try {
            service.findByNameContaining(null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByNameContainingFails1() {
        try {
            service.findByNameContaining("name", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByNameContainingFails2() {
        try {
            service.findByNameContaining("", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByDistance() {
        double latitude = 41.418204100;
        double longitude = 2.181243680;
        // Indiquem radi de 5 km des del punt indicat
        int distance = 5;
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByLocation(latitude, longitude, distance, request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 1);
        assertTrue(page.getNumberOfElements() >= 1);
        assertTrue(page.getTotalPages() >= 1);
        // Indiquem radi de 50 km des del punt indicat
        distance = 50;
        page = service.findByLocation(latitude, longitude, distance, request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 3);
        assertEquals(page.getNumberOfElements(), 2);
        assertTrue(page.getTotalPages() >= 2);
    }

    @Test
    public void findByDistanceFails() {
        try {
            service.findByLocation(0, 0, 0, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }

    }

    @Test
    public void findByDistanceFails1() {
        try {
            service.findByLocation(1, 0, 0, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByDistanceFails2() {
        try {
            service.findByLocation(1, 1, 0, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByDistanceFails3() {
        try {
            service.findByLocation(1, 1, 1, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void validateMandatoryDataFails() {
        try {
            service.save(null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void validateMandatoryDataFails1() {
        Center center = Center.Builder.aCenter()
                .withId("id").build();
        try {
            service.save(center);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(DataRequiredException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void validateMandatoryDataFails2() {
        Center center = Center.Builder.aCenter()
                .withId("id")
                .withName("nom").build();
        try {
            service.save(center);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(DataRequiredException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void validateMandatoryDataFails3() {
        Center center = Center.Builder.aCenter()
                .withId("id")
                .withName("nom")
                .withType(Type.of("EINF1C", "Educació infantil de 1r cicle")).build();
        try {
            service.save(center);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(DataRequiredException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void validateMandatoryDataFails4() {
        Center center = Center.Builder.aCenter()
                .withId("id")
                .withName("nom")
                .withType(Type.of("EINF1C", "Educació infantil de 1r cicle"))
                .withAddress("via").build();
        try {
            service.save(center);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(DataRequiredException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void validateMandatoryDataFails5() {
        Center center = Center.Builder.aCenter()
                .withId("id")
                .withName("nom")
                .withType(Type.of("EINF1C", "Educació infantil de 1r cicle"))
                .withAddress("via")
                .withPostalCode("codiPostal").build();
        try {
            service.save(center);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(DataRequiredException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void validateMandatoryDataFails6() {
        Center center = Center.Builder.aCenter()
                .withId("id")
                .withName("nom")
                .withType(Type.of("EINF1C", "Educació infantil de 1r cicle"))
                .withAddress("via")
                .withPostalCode("codiPostal")
                .withCity(City.of("0", "city")).build();
        try {
            service.save(center);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(DataRequiredException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void validateMandatoryDataFails7() {
        Center center = Center.Builder.aCenter()
                .withId("id")
                .withName("nom")
                .withType(Type.of("EINF1C", "Educació infantil de 1r cicle"))
                .withAddress("via")
                .withPostalCode("codiPostal")
                .withCity(City.of("0", "city"))
                .withRegion(Region.of("1", "region")).build();
        try {
            service.save(center);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(DataRequiredException.class, e.getClass());
        }
    }

    @Test
    @WithMockUser(username = "test", roles = {PersistenceConstants.ADMIN})
    public void validateMandatoryDataFails8() {
        Center center = Center.Builder.aCenter()
                .withId("id")
                .withName("nom")
                .withType(Type.of("EINF1C", "Educació infantil de 1r cicle"))
                .withAddress("via")
                .withPostalCode("codiPostal")
                .withCity(City.of("0", "city"))
                .withRegion(Region.of("1", "region"))
                .withLastModificationDate(new Date()).build();
        try {
            service.save(center);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(DataRequiredException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorial() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByTerritorial("1", request);
        assertNotNull(page);
        assertEquals(page.getTotalElements(), 4);
        assertEquals(page.getNumberOfElements(), 2);
        assertEquals(page.getTotalPages(), 2);
    }

    @Test
    public void findByNature() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByNature("1", request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 4);
        assertEquals(page.getNumberOfElements(), 2);
        assertTrue(page.getTotalPages() >= 2);
    }

    @Test
    public void findByOwner() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByOwner("1", request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 4);
        assertEquals(page.getNumberOfElements(), 2);
        assertTrue(page.getTotalPages() >= 2);
    }

    @Test
    public void findByTerritorialFails() {
        try {
            service.findByTerritorial(null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialFails1() {
        try {
            service.findByTerritorial("city", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialFails2() {
        try {
            service.findByTerritorial("", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByNatureFails() {
        try {
            service.findByNature(null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByNatureFails1() {
        try {
            service.findByNature("nature", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByNatureFails2() {
        try {
            service.findByNature("", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByOwnerFails() {
        try {
            service.findByOwner(null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByOwner1() {
        try {
            service.findByOwner("owner", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByOwner2() {
        try {
            service.findByOwner("", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerm() {
        PageRequest request = PageRequest.of(0, 2);
        Page<Center> page = service.findByTerm("vella", request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 1);
        assertEquals(page.getNumberOfElements(), 1);
        assertTrue(page.getTotalPages() >= 1);
    }

    @Test
    public void findByTerritorialAndTerm() {
        PageRequest request = PageRequest.of(0, 4);
        Page<Center> page = service.findByTerritorialAndTerm("1", "prim", request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 3);
        assertEquals(page.getNumberOfElements(), 3);
        assertTrue(page.getTotalPages() >= 1);
    }

    @Test
    public void findByTerritorialAndName() {
        PageRequest request = PageRequest.of(0, 4);
        Page<Center> page = service.findByTerritorialAndName("0143", "Vila-seca", request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 1);
        assertEquals(page.getNumberOfElements(), 1);
        assertTrue(page.getTotalPages() >= 1);
    }

    @Test
    public void findByTerritorialAndNameFails() {
        try {
            service.findByTerritorialAndName(null, null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialAndNameFails1() {
        try {
            service.findByTerritorialAndName("", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialAndNameFails2() {
        try {
            service.findByTerritorialAndName("1", null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialAndNameFails3() {
        try {
            service.findByTerritorialAndName("1", "", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByTerritorialAndNameFails4() {
        try {
            service.findByTerritorialAndName("1", "name", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByName() {
        PageRequest request = PageRequest.of(0, 4);
        Page<Center> page = service.findByName("Vila-seca", request);
        assertNotNull(page);
        assertTrue(page.getTotalElements() >= 1);
        assertEquals(page.getNumberOfElements(), 1);
        assertTrue(page.getTotalPages() >= 1);
    }

    @Test
    public void findByNameFails() {
        try {
            service.findByName(null, null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByNameFails1() {
        try {
            service.findByName("", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void findByNameFails2() {
        try {
            service.findByName("sss", null);
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

    @Test
    public void count() {
        long count = service.count();
        assertTrue(count >= 3);
    }

    @Test
    public void countByTerritorial() {
        long count = service.countByTerritorial("1");
        assertTrue(count >= 3L);
    }

    @Test
    public void countByTerritorialFails() {
        try {
            service.countByTerritorial("");
            fail("Test should fail");
        } catch (Exception e) {
            assertEquals(IllegalArgumentException.class, e.getClass());
        }
    }

}
