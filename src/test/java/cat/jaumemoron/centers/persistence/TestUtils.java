package cat.jaumemoron.centers.persistence;

import cat.jaumemoron.centers.persistence.domain.*;

import java.util.Date;

public class TestUtils {

    public static Center getCenter() {
        String id = "id";
        String nom = "nom";
        String tipus = "1. Llar d'infants";
        String via = "via";
        String codiPostal = "codiPostal";
        String region = "region";
        String telefon1 = "telefon1";
        String fax = "fax";
        String email1 = "email1";
        String email2 = "email2";
        Date lastModificationDate = new Date();
        return Center.Builder.aCenter()
                .withId(id)
                .withName(nom)
                .withType(Type.of(tipus, tipus))
                .withAddress(via)
                .withPostalCode(codiPostal)
                .withCity(City.of("1", "city"))
                .withRegion(Region.of("1", region))
                .withEmail(email1)
                .withEmail(email2)
                .withPhone(telefon1)
                .withFax(fax)
                .withLastModificationDate(lastModificationDate)
                .withOwner(Owner.of("1", "owner1"))
                .withNature(Nature.of("1", "nature1"))
                .withTerritorial(Territorial.of("1", "territorial1"))
                .withLocation(41.561752471, 1.185315112).build();
    }
}
