package cat.jaumemoron.centers.persistence;

import cat.jaumemoron.centers.persistence.utils.PersistenceUtils;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersistenceUtilsTest {

    @Test
    public void getSeason() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/07/2018")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/08/2018")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/09/2018")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/10/2018")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/11/2018")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/12/2018")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/01/2019")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/02/2019")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/03/2019")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/04/2019")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/05/2019")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/06/2019")), "2018-2019");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/07/2019")), "2019-2020");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/08/2019")), "2019-2020");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/09/2019")), "2019-2020");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/10/2019")), "2019-2020");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/10/2015")), "2015-2016");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/10/2016")), "2016-2017");
        assertEquals(PersistenceUtils.getSeason(sdf.parse("01/10/2017")), "2017-2018");
    }
}
